<?php
use \AFE\Model\User;
return array(
    'acl' => array(
        'role' => array (
            // role -> multiple parents
            User::ROLE_GUEST   => null,
            User::ROLE_MEMBER  => array(User::ROLE_GUEST),
            User::ROLE_COLLABORATOR => array(User::ROLE_MEMBER),
            User::ROLE_EDITOR => array(User::ROLE_MEMBER),
            User::ROLE_ADMIN   => null,
        ),
        'resource' => array (
            // resource -> single parent
            'home'          => null, // home controller
            'marker'        => null, // marker controller
            'editor'        => null, // editor controller
            'user'          => null, // user controller
            'kml'           => null, // kml controller
            'api-user'      => null, // user controller
            'api-record'    => null, // record controller
            'api-grid'      => 'api-record', // grid controller
            'api-territory' => 'api-record', // territory controller
            'api-status'    => 'api-record', // status controller
            'api-search'    => 'api-record', // search controller
            'api-import'    => 'api-record', // import controller
            'api-taxon'     => 'api-record', // taxon controller
            'admin'         => null, // admin controller
        ),
        'deny'  => array (
            // array('role', 'resource', array('permission-1', 'permission-2', ...)),
            array(User::ROLE_GUEST, null, null), // null as parameter means all
            array(User::ROLE_MEMBER, 'admin', null)
        ),
        'allow' => array (
            // array('role', 'resource', array('permission-1', 'permission-2', ...)),
            array(User::ROLE_GUEST, 'user', array('login', 'reset', 'logout')),
            array(User::ROLE_GUEST, 'api-user', null),
            array(User::ROLE_MEMBER, 'kml', null),
            array(User::ROLE_MEMBER, 'api-record', null),
            array(User::ROLE_MEMBER, 'api-import', null),
            array(User::ROLE_MEMBER, 'api-taxon', null),
            array(User::ROLE_MEMBER, 'api-grid', null),
            array(User::ROLE_MEMBER, 'api-territory', null),
            array(User::ROLE_MEMBER, 'api-search', null),
            array(User::ROLE_MEMBER, 'api-status', null),
            array(User::ROLE_COLLABORATOR, 'marker', null),
            array(User::ROLE_EDITOR, 'editor', null),
            array(User::ROLE_ADMIN, null, null), // the admin can do anything with the accounts
        ),
        'resource_aliases' => array (
            'AFE\Controller\Home'      => 'home',
            'AFE\Controller\Marker'    => 'marker',
            'AFE\Controller\User'      => 'user',
            'AFE\Controller\Kml'       => 'kml',
            'API\Controller\User'      => 'api-user',
            'API\Controller\Record'    => 'api-record',
            'API\Controller\Grid'      => 'api-grid',
            'API\Controller\Territory' => 'api-territory',
            'API\Controller\Taxon'     => 'api-taxon',
            'API\Controller\Status'    => 'api-status',
            'API\Controller\Search'    => 'api-search',
            'API\Controller\Import'    => 'api-import'
        ),
        // List of modules to apply the ACL. This is how we can specify if we have to protect the pages in our current module.
        'modules' => array (
            'AFE',
            'API'
        ),
    )
);
