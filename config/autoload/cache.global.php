<?php
return array(
    'cache' => array(
        'adapter' => array(
            'name' => 'filesystem',
            'options' => array (
                'cache_dir' => 'data/cache',
            )
        ),
        'plugins' => array(
            'exception_handler' => array(
                'throw_exceptions' => false
            ),
            'serializer' => array (
                'serializer' => 'Zend\Serializer\Adapter\PhpSerialize',
            )
        )
    ),
);