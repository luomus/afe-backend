<?php

return array(
    'db' => array(
        'adapters' => array(
            'default' => array(
                'driver'            => 'OCI8',
                'charset'           => 'AL32UTF8',
                'persistent'        => true
            ),
        )
    )
);
