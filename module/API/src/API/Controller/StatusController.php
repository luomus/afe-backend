<?php
namespace API\Controller;

use AFE\Db\Table\StatusTable;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class StatusController  extends AbstractRestfulController {

    public function getList() {
        $statusTable = new StatusTable();
        $results = $statusTable->fetchAll()->toArray();

        return new JsonModel(
            array_reverse($results)
        );
    }

}
