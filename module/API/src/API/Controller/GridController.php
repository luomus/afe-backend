<?php
namespace API\Controller;

use AFE\Db\Table\GridTable;
use AFE\Query\Criteria;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class GridController extends AbstractRestfulController {

    public function getList() {
        $territoryTable = new GridTable();
        $criteria       = new Criteria();
        $criteria->setCriteriaFromRequest($this->getRequest());

        $results = $territoryTable->fetchBy($criteria);

        return new JsonModel(
            $results
        );
    }

}
