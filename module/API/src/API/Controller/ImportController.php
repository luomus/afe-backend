<?php

namespace API\Controller;


use AFE\Form\Import;
use AFE\Model\User;
use AFE\Service\ExcelService;
use AFE\Service\ImportService;
use Zend\Filter\File\RenameUpload;
use Zend\Http\Response;
use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\Stdlib\RequestInterface as Request;
use Zend\View\Model\JsonModel;

class ImportController extends AbstractRestfulController {

    const IMPORT_PATH = './data/import/';

    private $importService;

    public function get($id) {
        $file = self::IMPORT_PATH . $id;
        if (!file_exists($file)) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(['error' => "Couldn't find imported file"]);
        }
        $data = [];
        $importService = $this->getImportService($file);
        try {
            $data = $importService->analyze();
            if ($data['matchedFields']) {
                return $this->update($id, $data);
            }
        } catch (\Exception $e) {
            $this->getResponse()->setStatusCode(406);
            $data['error'] = $e->getMessage();
        }

        return new JsonModel($data);
    }


    public function create($data) {
        $form = new Import();
        $form->setData($data);
        if (!$form->isValid()) {
            $this->response->setStatusCode(406);
            $params = [
                'error' => $form->getMessages()
            ];
            return new JsonModel($params);
        }
        $data = $form->getData();
        $target = self::IMPORT_PATH . $this->clearFileName($data['file']['name']);
        $filter = $this->getFilter($target);
        $result = $filter->filter($data['file']);
        $file = basename($result['tmp_name']);

        return new JsonModel(['id' => $file]);
    }


    public function update($id, $data) {
        $file = self::IMPORT_PATH . $id;
        if (!file_exists($file)) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(['error' => "Couldn't find imported file"]);
        }
        $action    = $this->getRequest()->getQuery('action', ImportService::ACTION_VALIDATE);
        $importService = $this->getImportService($file);
        $auth = $this->serviceLocator->get('Auth');
        /** @var User $user */
        $user = $auth->getIdentity();
        $importService->setDefaultUserID($user->getUserID());
        try {
            switch ($action) {
                case ImportService::ACTION_ANALYZE:
                    return $this->get($id);
                    break;
                case ImportService::ACTION_VALIDATE:
                    $data = $this->validate($data, $importService);
                    break;
                case ImportService::ACTION_SAVE:
                    $data = $this->save($data, $importService);
                    break;
                default:
                    $this->getResponse()->setStatusCode(406);
                    $data['error'] = 'Invalid action given';
                    break;
            }
        } catch (\Exception $e) {
            $this->getResponse()->setStatusCode(406);
            $data['error'] = $e->getMessage();
        }
        return new JsonModel($data);
    }

    private function validate($data, ImportService $importService) {
        return $importService->validate($data);
    }

    private function save($data, ImportService $importService) {
        return $importService->store($data);
    }

    private function clearFileName($filename) {
        return md5($filename);
    }

    private function getFilter($target) {
        $filter = new RenameUpload($target);
        $filter->setRandomize(true);

        return $filter;
    }

    /**
     * Process post data and call create
     *
     * @param Request $request
     * @return mixed
     */
    public function processPostData(Request $request)
    {
        if ($this->requestHasContentType($request, self::CONTENT_TYPE_JSON)) {
            $data = Json::decode($request->getContent(), $this->jsonDecodeType);
        } else {
            $data = array_merge_recursive($request->getPost()->toArray(), $request->getFiles()->toArray());
        }
        return $this->create($data);
    }

    protected function getImportService($file) {
        if ($this->importService === null) {
            $this->importService = new ImportService($file);
            $this->importService->setDefaultSheet($this->getRequest()->getQuery('sheet', 0));
            $this->importService->setSkipFirst($this->getRequest()->getQuery('skipFirst', 'true'));
        }
        return $this->importService;
    }

}
