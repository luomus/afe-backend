<?php
namespace API\Controller;

use AFE\Db\Table\RecordTable;
use AFE\Form\Record;
use AFE\Model\Record as RecordModel;
use AFE\Model\User;
use AFE\Query\Criteria;
use AFE\Service\UserService;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Zend\Paginator\Paginator;

class RecordController  extends AbstractRestfulController {

    protected static $users;

    public function getList() {
        $recordTable = new RecordTable();
        $criteria    = new Criteria();
        /** @var Request $request */
        $request     = $this->getRequest();
        $criteria->setCriteriaFromRequest($request);
        $records     = $recordTable->fetchBy($criteria);
        $auth        = $this->getServiceLocator()->get('auth');
        $currentUID  = $auth->hasIdentity() ? $auth->getIdentity()->getUserID() : null;
        $users       = $this->getAllUsers();
        $editorRoles = [User::ROLE_ADMIN, User::ROLE_EDITOR];
        $result      = [];
        foreach($records as $record) {
            /** @var \AFE\Model\Record $record */
            $data = $record->getArrayCopy();
            if ($data['userID'] === $currentUID) {
                $data['userType'] = 'current-user';
            } else {
                $data['userType'] = (isset($users[$data['userID']])
                    && in_array($users[$data['userID']]['role'], $editorRoles)) ? 'editor' : 'collaborator';
            }
            if (isset($users[$data['userID']])) {
                $data['username'] = $users[$data['userID']]['username'];
            }
            $result[] = $data;
        }
        return new JsonModel(
            $result
            //$records
        );
    }

    public function get($id) {
        $recordTable = new RecordTable();
        $record = $recordTable->get($id);
        $record = $record instanceof RecordModel ? $record->getArrayCopy() : null;
        return new JsonModel(
            $record
        );
    }

    public function create($data) {
        /** @var Record $form */
        $form = $this->serviceLocator->get('FormElementManager')->get('AFE\Form\Record');
        $form->setData($data);
        /** @var Response $response */
        $response = $this->getResponse();

        if ($form->isValid()) {
            $recordTable = new RecordTable();
            $id = $recordTable->save($form->getData());
        } else {
            $response->setStatusCode(400);

            return new JsonModel(['error' => $form->getMessages()]);
        }
        return $this->get($id);
    }

    public function update($id, $data) {
        /** @var Record $form */
        $form = $this->serviceLocator->get('FormElementManager')->get('AFE\Form\Record');
        $recordTable = new RecordTable();
        /** @var RecordModel $record */
        $record = $recordTable->get($id);
        $form->bind($record);
        $form->setData($data);
        if (!$form->isValid()) {
            /** @var Response $response */
            $response = $this->getResponse();
            $response->setStatusCode(400);

            return new JsonModel(['error' => $form->getMessages()]);
        }
        $recordTable->save($record);

        return new JsonModel($record->getArrayCopy());
    }

    public function delete($id) {
        $recordTable = new RecordTable();
        $recordTable->deleteAlbum($id);
        /** @var Response $response */
        $response = $this->getResponse();
        $response->setStatusCode(204);

        return $response;
    }

    private function getAllUsers() {
        if (!self::$users) {
            $userService = new UserService();
            $users = $userService->getAll();
            $result = [];
            foreach ($users as $user) {
                /** @var $user User */
                $result[$user->getUserID()] = [
                    'username' => $user->getUsername(),
                    'role' => $user->getRole()
                ];
            }
            self::$users = $result;
        }
        return self::$users;
    }

}
