<?php

namespace API\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Zend\Form\FormInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Http\Response;
use AFE\Controller\UserController as AuthService;

class UserController extends AbstractRestfulController {

    public function getList() {
        $auth = $this->getServiceLocator()->get('auth');
        if (!$auth->hasIdentity()) {
            $required = $this->getRequest()->getQuery('require', false);
            if ($required) {
                $this->response->setStatusCode(Response::STATUS_CODE_401);
            }
            return new JsonModel([]);
        }
        /** @var \AFE\Model\User $user */
        $user = $auth->getIdentity();
        return new JsonModel(['user' => $user->getArrayCopy()]);
    }

    public function create($data) {
        $view = new JsonModel();
        /** @var Response $response */
        $form    = $this->getLoginForm();
        $form->setData($data);
        if (!$form->isValid()) {
            $view->setVariables(['error' => $form->getMessages()]);
            return $view;
        }
        $data = $form->getData(FormInterface::VALUES_AS_ARRAY);
        /** @var AuthenticationService $auth */
        $auth = $this->serviceLocator->get('Auth');
        $auth->getAdapter()
            ->setIdentity($data['username'])
            ->setCredential($data['password']);
        $result = $auth->authenticate();
        if ($result->isValid()) {
            return $view->setVariables(['user' => $result->getIdentity()->getArrayCopy()]);
        }
        $view->setVariable('error', ['generic' => AuthService::getResultMessage($result)]);

        return $view;
    }

    public function delete($id) {
        $auth = $this->serviceLocator->get('auth');
        $auth->clearIdentity();

        return new JsonModel();
    }

    /**
     * @return \AFE\Form\User
     */
    private function getUserForm() {
        return $this->getServiceLocator()->get('FormElementManager')->get('AFE\Form\User');
    }

    private function getLoginForm() {
        $form = $this->getUserForm();
        $form->setLoginValidationGroup();

        return $form;
    }

}
