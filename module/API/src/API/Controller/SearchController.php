<?php
namespace API\Controller;

use AFE\Db\Table\TerritoryTable;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class SearchController  extends AbstractRestfulController {

    public function getList() {
        $query = $this->getRequest()->getQuery('q', '');
        /** @var \AFE\Service\SearchService $searchService */
        $searchService = $this->getServiceLocator()->get('AFE\Service\SearchService');

        return new JsonModel(
            $searchService->search($query)
        );
    }

}
