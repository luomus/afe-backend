<?php
namespace API\Controller;

use AFE\Db\Table\TaxonTable;
use AFE\Query\Criteria;
use AFE\Options\AfeConfig;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class TaxonController extends AbstractRestfulController {

    public function getList() {
        $taxonTable = new TaxonTable();
        $criteria   = new Criteria();
        /** @var AfeConfig $options */
        $config     = $this->getServiceLocator()->get('afeConfig');

        $criteria->setCriteriaFromRequest($this->getRequest());
        $criteria->addCriteria('volumeID', $config->getVolumeID());

        $results = $taxonTable->fetchBy($criteria);

        return new JsonModel(
            $results
        );
    }

}
