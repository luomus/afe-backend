<?php
namespace API\Controller;

use AFE\Db\Table\TerritoryTable;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class TerritoryController  extends AbstractRestfulController {

    public function getList() {
        $territoryTable = new TerritoryTable();
        $results = $territoryTable->fetchAll();

        return new JsonModel(
            $results
        );
    }

}
