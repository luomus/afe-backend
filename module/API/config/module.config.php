<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'API\Controller\Territory' => 'API\Controller\TerritoryController',
            'API\Controller\Grid' => 'API\Controller\GridController',
            'API\Controller\Taxon' => 'API\Controller\TaxonController',
            'API\Controller\Record' => 'API\Controller\RecordController',
            'API\Controller\User' => 'API\Controller\UserController',
            'API\Controller\Search' => 'API\Controller\SearchController',
            'API\Controller\Status' => 'API\Controller\StatusController',
            'API\Controller\Import' => 'API\Controller\ImportController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/[:controller[/:id]]',
                    'constraints' => array(
                        'id' => '[a-zA-Z0-9_-]*',
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'API\Controller',
                        'controller' => 'Index',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
);
