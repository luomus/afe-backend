<?php

namespace AFE\InputFilter;

use AFE\Filter\RegionToRegionID;
use AFE\Filter\StatusToStatusID;
use AFE\Filter\TaxonToTaxonID;
use AFE\Validator\InDatabase;
use Zend\Filter\Boolean;
use Zend\InputFilter\InputFilter;
use Zend\Stdlib\ArrayUtils;
use Zend\Validator;
use Zend\InputFilter\Exception;

/**
 * Class RecordFilter validates & filters records
 * @package AFE\InputFilter
 */
class RecordFilter extends InputFilter
{

    protected $statusFilter;
    protected $taxonFilter;
    protected $regionFilter;
    protected $booleanFilter;

    public function __construct()
    {
        $this->add([
            'name' => 'taxonID',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'messages' => array(
                            Validator\NotEmpty::IS_EMPTY => 'Missing taxon information.',
                        ),
                    ),

                ],
                [
                    'name' => 'AFE\Validator\InDatabase',
                    'options' => [
                        'table' => 'EM_TAXON',
                        'column' => 'TAXONID',
                        'distinct' => true,
                        'messages' => array(
                            InDatabase::NOT_FOUND => "'%value%' doesn't appear to be a taxon that we have in database.",
                        ),
                    ]
                ]
            ]
        ]);
        $this->add([
            'name' => 'quadratID',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'messages' => array(
                            Validator\NotEmpty::IS_EMPTY => 'Missing quadrat information.',
                        ),
                    ),

                ],
                [
                    'name' => 'AFE\Validator\InDatabase',
                    'options' => [
                        'table' => 'REGION_POLYGON',
                        'column' => 'QUADRAT',
                        'distinct' => true,
                        'messages' => array(
                            InDatabase::NOT_FOUND => "'%value%' doesn't appear to be valid quadrat.",
                        ),
                    ]
                ]
            ]
        ]);
        $this->add([
            'name' => 'statusID',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'messages' => array(
                            Validator\NotEmpty::IS_EMPTY => 'Missing status information.',
                        ),
                    ),

                ],
                [
                    'name' => 'AFE\Validator\InDatabase',
                    'options' => [
                        'table' => 'EM_STATUS',
                        'column' => 'STATUSID',
                        'messages' => array(
                            InDatabase::NOT_FOUND => "'%value%' doesn't appear to be valid status.",
                        ),
                    ]
                ]
            ]
        ]);
        $this->add([
            'name' => 'regionID',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'messages' => array(
                            Validator\NotEmpty::IS_EMPTY => 'Missing territory information.',
                        ),
                    ),

                ],
                [
                    'name' => 'AFE\Validator\InDatabase',
                    'options' => [
                        'table' => 'EM_REGION',
                        'column' => 'REGIONID',
                        'messages' => array(
                            InDatabase::NOT_FOUND => "Invalid territory.",
                        ),
                    ]
                ]
            ]
        ]);
        $this->add([
            'name' => 'userID',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'messages' => array(
                            Validator\NotEmpty::IS_EMPTY => 'Missing userID.',
                        ),
                    ),

                ]
            ]
        ]);
        $this->add([
            'name' => 'notes',
            'required' => false,
        ]);
        $this->add([
            'name' => 'accuracy_location',
            'required' => false,
        ]);
        $this->add([
            'name' => 'accuracy_taxon',
            'required' => false,
        ]);
    }

    public function setData($data)
    {
        if (!is_array($data) && !$data instanceof \Traversable) {
            throw new Exception\InvalidArgumentException(sprintf(
                '%s expects an array or Traversable argument; received %s',
                __METHOD__,
                (is_object($data) ? get_class($data) : gettype($data))
            ));
        }
        if (is_object($data) && !$data instanceof \ArrayAccess) {
            $data = ArrayUtils::iteratorToArray($data);
        }
        if (isset($data['status'])) {
            $data = $this->getStatusFilter()->filter($data);
        }
        if (isset($data['taxon'])) {
            $data = $this->getTaxonFilter()->filter($data);
        }
        if (isset($data['region'])) {
            $data = $this->getRegionFilter()->filter($data);
        }
        if (isset($data['uncertain_location']) && $data['uncertain_location'] !== '') {
            $val = $this->getBooleanFilter()->filter($data['uncertain_location']);
            $data['accuracy_location'] = $val ? 0 : 1;
        }
        if (isset($data['uncertain_taxon']) && $data['uncertain_taxon'] !== '') {
            $val = $this->getBooleanFilter()->filter($data['uncertain_taxon']);
            $data['accuracy_taxon'] = $val ? 0 : 1;
        }
        if (!isset($data['accuracy_taxon']) || $data['accuracy_taxon'] === '') {
            $data['accuracy_taxon'] = 1;
        }
        if (!isset($data['accuracy_location']) || $data['accuracy_location'] === '') {
            $data['accuracy_location'] = 1;
        }
        return parent::setData($data);
    }

    public function getBooleanFilter() {
        if ($this->booleanFilter === null) {
            $this->booleanFilter = new Boolean();
        }
        return $this->booleanFilter;
    }

    public function getTaxonFilter() {
        if ($this->taxonFilter === null) {
            $this->taxonFilter = new TaxonToTaxonID();
        }
        return $this->taxonFilter;
    }

    public function getRegionFilter() {
        if ($this->regionFilter === null) {
            $this->regionFilter = new RegionToRegionID();
        }
        return $this->regionFilter;
    }

    public function getStatusFilter() {
        if ($this->statusFilter === null) {
            $this->statusFilter = new StatusToStatusID();
        }
        return $this->statusFilter;
    }
}
