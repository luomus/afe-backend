<?php

namespace AFE\Authentication\Adapter;


use AFE\Model\User;
use Zend\Authentication\Adapter\DbTable\CallbackCheckAdapter;
use Zend\Authentication\Result as AuthenticationResult;
use Zend\Crypt\Password\Bcrypt;
use Zend\Db\Adapter\Adapter as DbAdapter;

class AfeUserAdapter extends CallbackCheckAdapter {

    public function __construct(
        DbAdapter $zendDb,
        $tableName = null,
        $identityColumn = null,
        $credentialColumn = null
    ) {
        parent::__construct(
            $zendDb,
            $tableName,
            $identityColumn,
            $credentialColumn,
            $this->getCredentialValidationCallback()
        );
    }

    public function authenticate() {
        try {
            $result = parent::authenticate();
        } catch (\Exception $e) {
            $this->authenticateResultInfo['code']       = AuthenticationResult::FAILURE_UNCATEGORIZED;
            $this->authenticateResultInfo['messages'][] = 'Connection to authentication server failed.';
            return $this->authenticateCreateAuthResult();
        }
        if ($result->isValid()) {
            return new AuthenticationResult(
                $this->authenticateResultInfo['code'],
                $this->getResultRowObject(null, $this->credentialColumn),
                $this->authenticateResultInfo['messages']
            );
        }
        return $result;
    }

    /**
     * getResultRowObject() - Returns the result row as a stdClass object
     *
     * @param  string|array $returnColumns
     * @param  string|array $omitColumns
     * @return User|bool
     */
    public function getResultRowObject($returnColumns = null, $omitColumns = null) {
        if (!$this->resultRow) {
            return false;
        }

        $result = $this->resultRow;
        if (null !== $returnColumns) {
            $returnColumns = (array) $returnColumns;
            $result = array_intersect_key($result, array_flip($returnColumns));
        } elseif (null !== $omitColumns) {
            $omitColumns = (array) $omitColumns;
            $result = array_diff_key($result, array_flip($omitColumns));
        }
        $returnObject = new User();
        $returnObject->exchangeArray($result);
        return $returnObject;
    }

    private function getCredentialValidationCallback() {
        return function($dbCredential, $requestCredential) {
            return (new Bcrypt())->verify($requestCredential, $dbCredential);
        };
    }

    public static function generateHash($password) {
        $crypt = new Bcrypt();
        $crypt->setCost(13);
        return $crypt->create($password);
    }

}
