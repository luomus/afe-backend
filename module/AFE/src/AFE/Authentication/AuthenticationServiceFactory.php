<?php
/**
 * Created by IntelliJ IDEA.
 * User: ville_000
 * Date: 17.10.2014
 * Time: 11:09
 */

namespace AFE\Authentication;

use AFE\Authentication\Adapter\AfeUserAdapter as AuthAdapter;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AuthenticationServiceFactory implements FactoryInterface {

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter   = $serviceLocator->get('default');
        $authAdapter = new AuthAdapter(
            $dbAdapter,
            'EM_USER',
            'USERNAME',
            'PASSWD'
        );
        $storage = new Session('AFE_Auth');

        return new AuthenticationService($storage, $authAdapter);
    }
}
