<?php

namespace AFE\Db\TableGateway;

use AFE\Model\Grid;
use AFE\Model\Record;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Delete;
use Zend\Db\Sql\Insert;
use Zend\Db\Sql\Update;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\TableGateway\Feature;

class RecordGateway extends AbstractTableGateway {

    const DATETIME_FORMAT = 'Y-m-d H:i:s';
    private static $altered = false;

    public function __construct() {
        $this->table = 'EM_RECORD';
        $this->featureSet = new Feature\FeatureSet();
        $this->featureSet->addFeature(new Feature\GlobalAdapterFeature());
        $this->featureSet->addFeature(new Feature\SequenceFeature('RECORDID','EM_RECORD_SEQ'));
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new Record());
        $this->initialize();
        if (!self::$altered) {
            self::$altered = true;
            $this->adapter->query("ALTER SESSION SET NLS_DATE_FORMAT='YYYY-MM-DD HH24:MI:SS'")->execute();
        }
    }

} 