<?php

namespace AFE\Db\TableGateway;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Delete;
use Zend\Db\Sql\Insert;
use Zend\Db\Sql\Update;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\TableGateway\Feature;

class RegionPolygonGateway extends AbstractTableGateway {

    public function __construct() {
        $this->table = 'REGION_POLYGON';
        $this->featureSet = new Feature\FeatureSet();
        $this->featureSet->addFeature(new Feature\GlobalAdapterFeature());
        $this->resultSetPrototype = new ResultSet();
        $this->initialize();
    }

    protected function executeInsert(Insert $insert) {
        throw new \Exception("Cannot add new region polygon this way");
    }

    protected function executeUpdate(Update $update) {
        throw new \Exception("Cannot update region polygon");
    }

    protected function executeDelete(Delete $delete) {
        throw new \Exception("Cannot delete region polygon");
    }

} 