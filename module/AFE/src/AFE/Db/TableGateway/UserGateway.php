<?php

namespace AFE\Db\TableGateway;


use AFE\Model\User;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\TableGateway\Feature;

class UserGateway extends AbstractTableGateway {

    public function __construct() {
        $this->table = 'EM_USER';
        $this->featureSet = new Feature\FeatureSet();
        $this->featureSet->addFeature(new Feature\GlobalAdapterFeature());
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new User());
        $this->initialize();
    }

    /**
     * @param array|User $set
     * @return int
     */
    public function insert($set) {
        if ($set instanceof User) {
            $set = $set->getArrayCopy(true);
        }
        return parent::insert($set);
    }

    public function update($set, $where = null) {
        if ($set instanceof User) {
            $set = $set->getArrayCopy(true);
        }
        return parent::update($set, $where);
    }

} 