<?php

namespace AFE\Db\Table;


use AFE\Db\TableGateway\TerritoryGateway;

class TerritoryTable {

    protected $tableGateway;

    public function __construct() {
        $this->tableGateway = new TerritoryGateway();
    }

    public function fetchAll() {
        $results = $this->tableGateway->select(['AFEAREA' => 1]);
        return $results;
    }

}