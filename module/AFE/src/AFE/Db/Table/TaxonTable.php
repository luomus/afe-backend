<?php

namespace AFE\Db\Table;

use AFE\Db\TableGateway\TaxonGateway;
use AFE\Query\Criteria;

class TaxonTable {

    protected $tableGateway;

    public function __construct() {
        $this->tableGateway = new TaxonGateway();
    }

    public function get($id) {
        $taxa = $this->tableGateway->select(['TAXONID' => $id]);
        foreach ($taxa as $taxon) {
            return $taxon;
        }
        return null;
    }

    public function fetchAll() {
        return $this->tableGateway->select();
    }

    public function fetchBy(Criteria $criteria) {
        $criteria->setWhiteList([
            'volumeID' => 'VOLUMEID',
            'taxonName' => 'TAXONNAME',
            'taxonID' => 'TAXONID',
            'parentID' => 'PARENTFK'
        ]);

        return $this->tableGateway->select($criteria->getQueryCriteria());
    }

}
