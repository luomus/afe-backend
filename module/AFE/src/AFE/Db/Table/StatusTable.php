<?php

namespace AFE\Db\Table;


use AFE\Db\TableGateway\StatusGateway;

class StatusTable {

    protected $tableGateway;

    public function __construct() {
        $this->tableGateway = new StatusGateway();
    }

    public function fetchAll() {
        return $this->tableGateway->select();
    }

}
