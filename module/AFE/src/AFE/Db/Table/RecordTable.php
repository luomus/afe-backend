<?php

namespace AFE\Db\Table;

use AFE\Db\TableGateway\GridGateway;
use AFE\Db\TableGateway\RecordGateway;
use AFE\Model\Record;
use AFE\Query\Criteria;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class RecordTable {

    protected $tableGateway;

    public function __construct() {
        $this->tableGateway = new RecordGateway();
    }

    public function fetchAll() {
        return $this->tableGateway->select();
    }

    public function fetchBy(Criteria $criteria, $orderBy = null, $paginated = false) {
        if ($criteria->hasCriteria('territory')) {
            $criteria->setSelectCriteria(
                'QUADRATID',
                $this->getTerritorySelect($criteria->getCriteria('territory'))
            );
        }
        $criteria->setWhiteList([
            'taxon'   => 'TAXONID',
            'quadrat' => 'QUADRATID',
            'status'  => 'STATUSID',
            'userID'  => 'USERID'
        ]);
        $where = $criteria->getSqlWhereCriteria();
        if ($paginated) {
            $select = new Select($this->tableGateway->table);
            $paginatorAdapter = new DbSelect(
                $select,
                $this->tableGateway->getAdapter(),
                $this->tableGateway->getResultSetPrototype()
            );
            $paginator = new Paginator($paginatorAdapter);

            return $paginator;
        }
        /** @var Select $select */
        $select = $this->tableGateway->getSql()->select();
        $select->where($where);
        if ($orderBy !== null) {
            $select->order($orderBy);
        }

        return $this->tableGateway->selectWith($select);
    }

    private function getTerritorySelect($territory) {
        $gridGateway = new GridGateway();
        $select = $gridGateway->getSql()->select();
        $select->columns(['QUADRAT'])->where->equalTo('REGION', $territory);

        return $select;
    }

    public function get($id) {
        $records = $this->tableGateway->select(['RECORDID' => $id]);
        foreach ($records as $record) {
            return $record;
        }
        return null;
    }

    public function save(Record $record) {
        $now = new \DateTime();
        $record->setCreated($now->format(RecordGateway::DATETIME_FORMAT));
        $data = $record->getArrayCopy(true);
        $data['ACCURACY_TAXON'] = (int) $data['ACCURACY_TAXON'];
        $data['ACCURACY_LOCATION'] = (int) $data['ACCURACY_LOCATION'];
        $id   = (int)$record->getRecordID();
        if ($id == 0) {
            unset($data['RECORDID']);
            $this->tableGateway->insert($data);

            return $this->tableGateway->getLastInsertValue();
        } else {
            if ($this->get($id)) {
                $this->tableGateway->update($data, ['RECORDID' => $id]);
            } else {
                throw new \Exception("Record id $id doesn't exist");
            }
        }
        return $record;
    }

    public function deleteAlbum($id) {
        return $this->tableGateway->delete(['RECORDID' => (int) $id]);
    }

}
