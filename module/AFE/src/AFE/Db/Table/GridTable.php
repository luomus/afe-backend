<?php

namespace AFE\Db\Table;

use AFE\Db\TableGateway\GridGateway;
use AFE\Query\Criteria;

class GridTable {

    protected $tableGateway;

    public function __construct() {
        $this->tableGateway = new GridGateway();
    }

    public function fetchAll() {
        return $this->tableGateway->select();
    }

    public function fetchBy(Criteria $criteria) {
        $criteria->setWhiteList(['territory' => 'REGION', 'quadrat' => 'QUADRAT']);

        return $this->tableGateway->select($criteria->getQueryCriteria());
    }

}
