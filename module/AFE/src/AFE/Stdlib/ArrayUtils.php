<?php

namespace AFE\Stdlib;


class ArrayUtils {

    public static function array_to_paths($array) {
        $return = array();
        foreach ($array as $key => $value) {
            self::line($return, $value, $key, 0);
        }

        return $return;
    }

    private static function line(&$result, $arr, $path, $level)
    {
        if (is_array($arr)) {
            foreach ($arr as $key => $value) {
                if (is_array($value)) {
                    self::line($result, $value, $path . '['.$key.']', $level + 1);
                } else {
                    $result[$path] = $value;
                }
            }
        }
    }
} 