<?php

namespace AFE\Stdlib;


trait CapitalizedArraySerializableTrait {

    /**
     * Exchange internal values from provided array
     *
     * @param array $array
     * @throws \Exception
     */
    public function exchangeArray(array $array)
    {
        $array = array_change_key_case($array, CASE_LOWER);
        $keys  = array_keys(get_object_vars($this));
        $keysLower = array_change_key_case(array_flip($keys), CASE_LOWER);
        $normalized = array_combine(
            array_flip($keysLower),
            $keys
        );
        foreach ($array as $key => $value) {
            if (!isset($normalized[$key])) {
                if (isset($specialMappings[$key]) &&
                    isset($normalized[$specialMappings[$key]])) {
                    $normalized[$key] = $specialMappings[$key];
                } else {
                    throw new \Exception("Unable to exchange key $key with " . get_class($this));
                }
            }
            $property = $normalized[$key];
            $this->$property = $value;
        }
    }


    /**
     * Return an array representation of the object
     *
     * @param bool $capitalize
     *
     * @return array
     */
    public function getArrayCopy($capitalize = false)
    {
        $values = get_object_vars($this);
        if ($capitalize) {
            $values = array_change_key_case($values, CASE_UPPER);
        }

        return $values;
    }

}