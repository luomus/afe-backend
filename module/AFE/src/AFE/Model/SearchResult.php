<?php

namespace AFE\Model;


class SearchResult implements \JsonSerializable {

    /** @var string */
    private $name;
    /** @var array */
    private $location;
    /** @var array */
    private $bounds;
    /** @var string */
    private $territory;
    /** @var string */
    private $quadrat;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getTerritory()
    {
        return $this->territory;
    }

    /**
     * @param string $territory
     */
    public function setTerritory($territory)
    {
        $this->territory = $territory;
    }

    /**
     * @return array
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param array $location
     */
    public function setLocation(array $location)
    {
        $this->location = $location;
    }

    /**
     * @return string
     */
    public function getQuadrat()
    {
        return $this->quadrat;
    }

    /**
     * @param string $quadrat
     */
    public function setQuadrat($quadrat)
    {
        $this->quadrat = $quadrat;
    }

    /**
     * @return array
     */
    public function getBounds()
    {
        return $this->bounds;
    }

    /**
     * @param array $bounds
     */
    public function setBounds($bounds)
    {
        $this->bounds = $bounds;
    }

    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
