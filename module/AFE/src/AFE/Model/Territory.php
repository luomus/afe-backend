<?php

namespace AFE\Model;

use AFE\Stdlib\CapitalizedArraySerializableTrait;
use Zend\Stdlib\ArraySerializableInterface;

class Territory implements ArraySerializableInterface
{

    use CapitalizedArraySerializableTrait;

    /** @var int */
    private $regionID;
    /** @var string */
    private $region;
    /** @var string */
    private $name;
    /** @var int */
    private $type;
    /** @var string */
    private $parent;
    /** @var boolean */
    private $afearea;
    /** @var string */
    private $description;

    /**
     * @return int
     */
    public function getTerritoryID()
    {
        return $this->regionID;
    }

    /**
     * @param int $territoryID
     */
    public function setTerritoryID($territoryID)
    {
        $this->regionID = $territoryID;
    }

    /**
     * @return string
     */
    public function getTerritory()
    {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setTerritory($region)
    {
        $this->region = $region;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param string $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return boolean
     */
    public function isAfearea()
    {
        return $this->afearea;
    }

    /**
     * @param boolean $afearea
     */
    public function setAfearea($afearea)
    {
        $this->afearea = $afearea;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

}