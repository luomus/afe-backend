<?php

namespace AFE\Model;

use AFE\Stdlib\CapitalizedArraySerializableTrait;
use Zend\Stdlib\ArraySerializableInterface;

class Grid implements ArraySerializableInterface {

    use CapitalizedArraySerializableTrait;

    /** @var string */
    private $region;
    /** @var string */
    private $quadrat;
    /** var string */
    private $polygon;

    /**
     * @return string
     */
    public function getTerritory()
    {
        return $this->region;
    }

    /**
     * @param string $territory
     */
    public function setTerritory($territory)
    {
        $this->region = $territory;
    }

    /**
     * @return string
     */
    public function getQuadrat()
    {
        return $this->quadrat;
    }

    /**
     * @param string $quadrat
     */
    public function setQuadrat($quadrat)
    {
        $this->quadrat = $quadrat;
    }

    /**
     * @return mixed
     */
    public function getPolygon()
    {
        return $this->polygon;
    }

    /**
     * @param mixed $polygon
     */
    public function setPolygon($polygon)
    {
        $this->polygon = $polygon;
    }

}