<?php

namespace AFE\Model;

use AFE\Stdlib\CapitalizedArraySerializableTrait;
use Zend\Stdlib\ArraySerializableInterface;

class Record implements ArraySerializableInterface {

    use CapitalizedArraySerializableTrait {
        exchangeArray as traitExchangeArray;
    }

    /** @var int */
    private $recordID;
    /** @var int */
    private $taxonID;
    /** @var int */
    private $quadratID;
    /** @var int */
    private $statusID;
    /** @var int */
    private $userID;
    /** @var boolean */
    private $accuracy_taxon = 1;
    /** @var boolean */
    private $accuracy_location = 1;
    /** @var string */
    private $notes;
    /** @var string */
    private $created;
    /** @var int */
    private $regionID;

    /**
     * @return int
     */
    public function getRecordID()
    {
        return $this->recordID;
    }

    /**
     * @param int $recordID
     */
    public function setRecordID($recordID)
    {
        $this->recordID = $recordID;
    }

    /**
     * @return int
     */
    public function getTaxonID()
    {
        return $this->taxonID;
    }

    /**
     * @param int $taxonID
     */
    public function setTaxonID($taxonID)
    {
        $this->taxonID = $taxonID;
    }

    /**
     * @return int
     */
    public function getQuadratID()
    {
        return $this->quadratID;
    }

    /**
     * @param int $quadratID
     */
    public function setQuadratID($quadratID)
    {
        $this->quadratID = $quadratID;
    }

    /**
     * @return int
     */
    public function getStatusID()
    {
        return $this->statusID;
    }

    /**
     * @param int $statusID
     */
    public function setStatusID($statusID)
    {
        $this->statusID = $statusID;
    }

    /**
     * @return int
     */
    public function getUserID()
    {
        return $this->userID;
    }

    /**
     * @param int $userID
     */
    public function setUserID($userID)
    {
        $this->userID = $userID;
    }

    /**
     * @return boolean
     */
    public function isAccuracyTaxon()
    {
        return (bool)$this->accuracy_taxon;
    }

    /**
     * @param boolean $accuracy_taxon
     */
    public function setAccuracyTaxon($accuracy_taxon)
    {
        $this->accuracy_taxon = $accuracy_taxon;
    }

    /**
     * @return boolean
     */
    public function isAccuracyLocation()
    {
        return (bool)$this->accuracy_location;
    }

    /**
     * @param boolean $accuracy_location
     */
    public function setAccuracyLocation($accuracy_location)
    {
        $this->accuracy_location = $accuracy_location;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param string $notes
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getRegionID()
    {
        return $this->regionID;
    }

    /**
     * @param int $regionID
     */
    public function setRegionID($regionID)
    {
        $this->regionID = $regionID;
    }

    public function exchangeArray(array $array) {
        if (isset($array['B_ROWNUM'])) {
            unset($array['B_ROWNUM']);
        }
        if (isset($array['NOTES']) && is_a($array['NOTES'], 'OCI-Lob')) {
            $array['NOTES'] = $array['NOTES']->load();
        }
        return $this->traitExchangeArray($array);
    }
}
