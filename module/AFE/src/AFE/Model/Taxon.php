<?php

namespace AFE\Model;

use AFE\Stdlib\CapitalizedArraySerializableTrait;
use Zend\Stdlib\ArraySerializableInterface;

class Taxon implements ArraySerializableInterface {

    use CapitalizedArraySerializableTrait;

    /** @var int */
    private $taxonID;
    /** @var int */
    private $parentFK;
    /** var string */
    private $taxonURI;
    /** var string */
    private $taxonName;
    /** var int */
    private $volumeID;

    /**
     * @return int
     */
    public function getTaxonID()
    {
        return $this->taxonID;
    }

    /**
     * @param int $taxonID
     */
    public function setTaxonID($taxonID)
    {
        $this->taxonID = $taxonID;
    }

    /**
     * @return int
     */
    public function getParentFK()
    {
        return $this->parentFK;
    }

    /**
     * @param int $parentFK
     */
    public function setParentFK($parentFK)
    {
        $this->parentFK = $parentFK;
    }

    /**
     * @return mixed
     */
    public function getTaxonURI()
    {
        return $this->taxonURI;
    }

    /**
     * @param mixed $taxonURI
     */
    public function setTaxonURI($taxonURI)
    {
        $this->taxonURI = $taxonURI;
    }

    /**
     * @return mixed
     */
    public function getTaxonName()
    {
        return $this->taxonName;
    }

    /**
     * @param mixed $taxonName
     */
    public function setTaxonName($taxonName)
    {
        $this->taxonName = $taxonName;
    }

    /**
     * @return mixed
     */
    public function getVolumeID()
    {
        return $this->volumeID;
    }

    /**
     * @param mixed $volumeID
     */
    public function setVolumeID($volumeID)
    {
        $this->volumeID = $volumeID;
    }



}
