<?php

namespace AFE\Model;

use AFE\Stdlib\CapitalizedArraySerializableTrait;
use Zend\Stdlib\ArraySerializableInterface;

class User implements ArraySerializableInterface
{

    use CapitalizedArraySerializableTrait {
        exchangeArray as traitExchangeArray;
    }

    const DEFAULT_ROLE = 'guest';
    const DEFAULT_ROLE_LOGIN = 'member';

    const ROLE_GUEST        = 'guest';
    const ROLE_MEMBER       = 'member';
    const ROLE_COLLABORATOR = 'collaborator';
    const ROLE_EDITOR       = 'editor';
    const ROLE_ADMIN        = 'admin';

    /** @var int */
    private $userID;
    /** @var string */
    private $username;
    /** @var string */
    private $email;
    /** @var string */
    private $passwd;
    /** @var string */
    private $token;
    /** @var string */
    private $lang;
    /** @var string */
    private $lastname;
    /** @var string */
    private $firstname;
    /** @var string */
    private $role = self::DEFAULT_ROLE;


    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getUserID()
    {
        return $this->userID;
    }

    /**
     * @param int $id
     */
    public function setUserID($id)
    {
        $this->userID = $id;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->passwd;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->passwd = $password;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstName
     */
    public function setFirstname($firstName)
    {
        $this->firstname = $firstName;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastName
     */
    public function setLastname($lastName)
    {
        $this->lastname = $lastName;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    public function exchangeArray(array $array) {
        if (isset($array['password'])) {
            $array['passwd'] = $array['password'];
            unset($array['password']);
        }
        return $this->traitExchangeArray($array);
    }
}
