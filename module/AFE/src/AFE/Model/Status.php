<?php
namespace AFE\Model;

use AFE\Stdlib\CapitalizedArraySerializableTrait;
use Zend\Stdlib\ArraySerializableInterface;

class Status implements ArraySerializableInterface {
    use CapitalizedArraySerializableTrait;

    private $statusID;
    private $statusText;

    /**
     * @return mixed
     */
    public function getStatusID()
    {
        return $this->statusID;
    }

    /**
     * @param mixed $statusID
     */
    public function setStatusID($statusID)
    {
        $this->statusID = $statusID;
    }

    /**
     * @return mixed
     */
    public function getStatusText()
    {
        return $this->statusText;
    }

    /**
     * @param mixed $statusText
     */
    public function setStatusText($statusText)
    {
        $this->statusText = $statusText;
    }



}
