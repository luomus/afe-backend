<?php
namespace AFE\Controller;

use AFE\Authentication\Adapter\AfeUserAdapter;
use AFE\Db\TableGateway\UserGateway;
use AFE\Model\User;
use AFE\Service\UserService;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Result;
use Zend\Form\FormInterface;
use Zend\Http\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class UserController extends AbstractActionController
{

    public function loginAction()
    {
        $view = new ViewModel();
        /** @var Request $request */
        $request = $this->getRequest();
        $form    = $this->getLoginForm();
        $view->setVariable('loginForm', $form);
        $view->setVariable('forgottenForm', $this->getForgotten());
        $view->setTerminal(true);

        if ($request->isPost()) {
            $form->setData($request->getPost());
            if (!$form->isValid()) {
                return $view;
            }
            $data = $form->getData(FormInterface::VALUES_AS_ARRAY);
            /** @var AuthenticationService $auth */
            $auth     = $this->serviceLocator->get('Auth');
            $auth->getAdapter()
                ->setIdentity($data['username'])
                ->setCredential($data['password']);
            $result = $auth->authenticate();
            if ($result->isValid()) {
                $goto = $request->getQuery('redirect','/');
                return $this->redirect()->toUrl($goto);
            } else {
                $view->setVariable('status', $this->_getResultMessage($result));
            }
        }

        return $view;
    }

    public function logoutAction() {
        $auth = $this->serviceLocator->get('auth');
        $auth->clearIdentity();

        return $this->redirectToHome();
    }

    public function resetAction() {
        /** @var Request $request */
        $view    = new ViewModel();
        $view->setTerminal(true);
        $request = $this->getRequest();
        $token   = $request->getQuery('token', false);
        $user    = $request->getQuery('user', false);
        $pwForm  = $this->getPasswordChangeForm();
        if (!$user || !$token) {
            return $this->redirectToHome();
        }

        $userGateway = new UserGateway();
        $user = $userGateway->select(['USERNAME' => $user])->current();
        $userService = new UserService();
        $userToken = $userService->getUserToken($user);

        if (!$user instanceof User || $userToken !== $token) {
            return $this->redirectToHome();
        }

        $user->setToken(null);
        if ($request->isPost()) {
            $password = $request->getPost('password');
            $postData = $request->getPost();
            $postData['username'] = $user;
            $pwForm->setData($postData);
            if ($pwForm->isValid()) {
                $userService->updateUser($user, $password);
                return $this->redirect()->toRoute('default',['controller' => 'user','action' => 'login']);
            }
        }
        $userService->updateUser($user);
        $view->setVariable('passwordForm', $pwForm);

        return $view;
    }

    private function _getResultMessage(Result $result) {
        return self::getResultMessage($result);
    }

    public static function getResultMessage(Result $result) {
        switch ($result->getCode()) {
            case Result::FAILURE_CREDENTIAL_INVALID:
            case Result::FAILURE_IDENTITY_NOT_FOUND:
            case Result::FAILURE_IDENTITY_AMBIGUOUS:
                return 'Invalid credentials given';
            case Result::FAILURE_UNCATEGORIZED:
                return $result->getMessages()[0];
        }
        return "An error occurred. Try again later";
    }

    private function redirectToHome() {
        return $this->redirect()->toRoute('default', array('action' => 'index', 'controller' => 'home'));
    }

    private function getPasswordChangeForm() {
        $form = $this->getUserForm();
        $form->setPWResetValidationGroup();

        return $form;
    }

    private function getLoginForm() {
        $form = $this->getUserForm();
        $form->setLoginValidationGroup();

        return $form;
    }

    private function getForgotten() {
        $form = $this->getUserForm();
        $form->setEmailValidationGroup();

        return $form;
    }

    /**
     * @return \AFE\Form\User
     */
    private function getUserForm() {
        return $this->serviceLocator->get('FormElementManager')->get('AFE\Form\User');
    }
}
