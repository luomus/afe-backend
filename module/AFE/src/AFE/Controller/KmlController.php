<?php
namespace AFE\Controller;


use AFE\Db\Table\TaxonTable;
use AFE\Model\Taxon;
use AFE\Model\User;
use AFE\Service\KmlService;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractActionController;

class KmlController extends AbstractActionController {

    public function indexAction() {
        /** @var Response $response */
        $territory = $this->getEvent()->getRouteMatch()->getParam('territory');
        $taxon     = $this->getRequest()->getQuery('taxon');
        $download  = $this->getRequest()->getQuery('download', false);
        $allUsers  = (bool) $this->getRequest()->getQuery('allUser', false);
        $response  = $this->getResponse();
        $onlyUsers = false;
        if ($territory === null) {
            $response->setStatusCode(404);
            return $response;
        }
        if (!$allUsers) {
            /** @var \Zend\Authentication\AuthenticationService $auth */
            $auth = $this->getServiceLocator()->get('auth');
            /** @var User $user */
            $user = $auth->getIdentity();
            $onlyUsers = $user->getUserID();
        }
        $kmlService = new KmlService();
        $kml        = $kmlService->getGrid($territory, $taxon, $onlyUsers);

        if ($download) {
            if (is_numeric($taxon)) {
                $taxonTable = new TaxonTable();
                $result = $taxonTable->get($taxon);
                if ($result instanceof Taxon) {
                    $taxon = $result->getTaxonName();
                }
            }
            $response->getHeaders()
                ->addHeaders(array('Content-type' => 'text/xml'))
                ->addHeaderLine('Content-Disposition', 'attachment; filename="' . $territory . ' ('.$taxon.').kml"')
                ->addHeaderLine('Content-Length', strlen($kml));
        } else {
            $response->getHeaders()->addHeaders(array('Content-type' => 'application/vnd.google-earth.kml+xml'));
        }

        $response->setContent($kml);
        return $response;
    }

}
