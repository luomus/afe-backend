<?php

namespace AFE\Controller;

use AFE\Db\TableGateway\UserGateway;
use AFE\Service\UserService;
use Zend\Http\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class AdminController extends AbstractActionController
{
    public function usersAction()
    {
        $userService = new UserService();
        $view         = new ViewModel();
        /** @var Request $request */
        $request      = $this->getRequest();
        $userId       = $request->getPost('userid', $request->getQuery('userid', null));
        $addForm      = $this->getAddForm();
        $editForm     = $this->getEditForm();
        $view->setVariable('userForm', $addForm);
        $view->setVariable('editForm', $editForm);
        /** @var Request $request */
        if ($request->isPost()) {
            if ($userId === null) {
                $form     = $addForm;
                $method   = 'addUser';
                $errorVar = 'userAddError';
            } else {
                $form     = $editForm;
                $method   = 'updateUser';
                $errorVar = 'userEditError';
                $form->bind($userService->getById($userId));
            }
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $userService->$method($form->getData());
            } else {
                $view->setVariable($errorVar, true);
            }
        } elseif ($userId !== null) {
            $editForm->bind($userService->getById($userId));
            $view->setTemplate('afe/admin/user-edit');
            $view->setTerminal(true);
        }
        $view->setVariable('users', $userService->getAll());

        return $view;
    }

    private function getEditForm() {
        $form = $this->getUserForm();
        $form->setEditValidationGroup();

        return $form;
    }

    private function getAddForm() {
        $form = $this->getUserForm();
        $form->setAddValidationGroup();

        return $form;
    }

    /**
     * @return \AFE\Form\User
     */
    private function getUserForm() {
        /** @var \AFE\Form\User $form */
        $form = $this->serviceLocator->get('FormElementManager')->get('AFE\Form\User');
        $elem = $form->get('username');
        $elem->setAttribute('placeholder', '');
        $elem->setOption('column-size', 'sm-12');
        return $form;
    }
}
