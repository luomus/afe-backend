<?php

namespace AFE\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class MarkerController extends AbstractActionController
{
    public function indexAction()
    {
        $view = new ViewModel();
        $view->setVariable('mapForm', $this->getMapToolForm());

        return $view;
    }

    private function getMapToolForm() {
        return $this->serviceLocator->get('FormElementManager')->get('AFE\Form\MapTool');
    }
}
