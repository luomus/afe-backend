<?php
namespace AFE\Query;

use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Http\Request;

class Criteria {

    private $isDirty = true;
    private $fields = [];
    private $query = [];
    private $whiteList = [];

    public function getQueryCriteria() {
        if (!$this->isDirty) {
            return $this->query;
        }
        foreach ($this->fields as $key => $value) {
            if (isset($this->whiteList[$key])) {
                $this->query[$this->whiteList[$key]] = $value;
            }
        }
        $this->isDirty = false;
        return $this->query;
    }

    public function getSqlWhereCriteria() {
        $where = new Where();
        $query = $this->getQueryCriteria();
        foreach ($query as $field => $value) {
            if ($value instanceof Select) {
                $where->and->in($field, $value);
            } else {
                $where->and->equalTo($field, $value);
            }
        }

        return $where;
    }

    public function setSelectCriteria($field, Select $select) {
        $this->query[$field] = $select;
    }

    public function setCriteriaFromRequest(Request $request) {
        $query = $request->getQuery();
        $this->setCriteriaFromArray($query->toArray());

        return $this;
    }

    public function setCriteriaFromArray(array $array) {
        $lowerArray    = array_change_key_case($array, CASE_LOWER);
        $this->fields  = array_merge($this->fields, $lowerArray);
        $this->isDirty = true;

        return $this;
    }

    public function setWhiteList($list) {
        $this->whiteList = array_change_key_case($list, CASE_LOWER);
    }

    public function addCriteria($field, $value) {
        $this->fields[strtolower($field)] = $value;
        $this->isDirty = true;
    }

    public function hasCriteria($field) {
        $field = strtolower($field);
        return isset($this->fields[$field]);
    }

    public function getCriteria($field) {
        if ($this->hasCriteria($field)) {
            $field = strtolower($field);

            return $this->fields[$field];
        }
        return null;
    }

}
