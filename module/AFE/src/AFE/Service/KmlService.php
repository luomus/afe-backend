<?php

namespace AFE\Service;


use AFE\Db\Table\RecordTable;
use AFE\Db\TableGateway\RegionPolygonGateway;
use AFE\Model\Record;
use AFE\Query\Criteria;
use Zend\Db\ResultSet\ResultSet;

class KmlService {

    private $editos;

    public function getGrid($territory, $taxon = null, $onlyUser = false) {
        $regionGateway = new RegionPolygonGateway();
        $results = $territory === 'all' ?
            $regionGateway->select() :
            $regionGateway->select(['REGION' => $territory]);
        $territoryRecords = [];
        if ($taxon !== null) {
            $territoryRecords = $this->getTerritoryRecords($territory, $taxon, $onlyUser);
        }

        return $this->generateKml($results, $territoryRecords, $territory);
    }

    private function initEditors()
    {
        if ($this->editos !== null) {
            return;
        }
        $userService  = new UserService();
        $this->editos = array_flip($userService->getAllEditors());
    }

    private function getTerritoryRecords($territory, $taxon, $onlyUser) {
        $this->initEditors();
        $recordTable = new RecordTable();
        $criteria    = new Criteria();
        if ($territory !== 'all') {
            $criteria->addCriteria('territory', $territory);
        }
        $criteria->addCriteria('taxon', $taxon);
        if ($onlyUser !== false) {
            $criteria->addCriteria('userID', $onlyUser);
        }
        $records = $recordTable->fetchBy($criteria, ['CREATED' => 'DESC']);
        $result = [];
        foreach ($records as $record) {
            /** @var Record $record */
            $quadrat  = $record->getQuadratID();
            $isEditor = $this->isEditor($record->getUserID());
            $accurate = $record->isAccuracyLocation() && $record->isAccuracyTaxon();
            $status   = (int) $record->getStatusID();
            if (isset($result[$quadrat]) &&
                (
                    (!$result[$quadrat]['isEditor'] && $isEditor) ||
                    ($accurate === true && !$result[$quadrat]['isEditor'] &&
                        (
                            $result[$quadrat]['accurate'] === false ||
                            $result[$quadrat]['status'] < $status
                        )
                    )
                )) {
                $result[$quadrat] = [
                    'status' => $status,
                    'isEditor' => $isEditor,
                    'accurate' => $accurate,
                ];
            } else if (!isset($result[$quadrat])) {
                $result[$quadrat] = [
                    'status' => $status,
                    'isEditor' => $isEditor,
                    'accurate' => $accurate,
                ];
            }
        }
        return $result;
    }

    private function isEditor($userID) {
        return isset($this->editos[$userID]);
    }

    private function generateKml(ResultSet $results, $records, $territory) {
        // Creates the Document.
        $dom = new \DOMDocument('1.0', 'UTF-8');

        // Creates the root KML element and appends it to the root document.
        $node = $dom->createElementNS('http://www.opengis.net/kml/2.2', 'kml');
        $parNode = $dom->appendChild($node);

        // Creates a KML Document element and append it to the KML element.
        $dnode = $dom->createElement('Document');
        $docNode = $parNode->appendChild($dnode);
        $style = $dom->createDocumentFragment();
        $style->appendXML($this->getStyle());
        $dnode->appendChild($style);

        // Iterates through the MySQL results, creating one Placemark for each row.
        foreach($results as $row)
        {
            // Creates a Placemark and append it to the Document.

            $node = $dom->createElement('Placemark');
            $placeNode = $docNode->appendChild($node);

            // Creates an id attribute and assign it the value of id column.
            $placeNode->setAttribute('id', $row['QUADRAT']);

            // Create name, and description elements and assigns them the values of the name and address columns from the results.
            $nameNode = $dom->createElement('name', $row['QUADRAT']);
            $placeNode->appendChild($nameNode);
            $descNode = $dom->createElement('description', $row['REGION']);
            $placeNode->appendChild($descNode);
            if (isset($records[$row['QUADRAT']])) {
                if ($records[$row['QUADRAT']]['status'] === 0) {
                    continue;
                }
                if ($records[$row['QUADRAT']]['accurate']) {
                    $style = 'status-' . $records[$row['QUADRAT']]['status'];
                } else {
                    $style = 'unsure';
                }

            } else {
                if ($territory === 'all') {
                    continue;
                }
                $style = 'grid';
            }
            $styleUrl = $dom->createElement('styleUrl', '#' . $style);
            $placeNode->appendChild($styleUrl);

            // Creates a Multi geometry element.
            $polygonNode = $dom->createElement('Polygon');
            $placeNode->appendChild($polygonNode);
            $outerBoundaryNode = $dom->createElement('outerBoundaryIs');
            $polygonNode->appendChild($outerBoundaryNode);
            $linearNode = $dom->createElement('LinearRing');
            $outerBoundaryNode->appendChild($linearNode);

            // Creates a coordinates polygon
            $coorNode = $dom->createElement('coordinates', $row['POLYGON']);
            $linearNode->appendChild($coorNode);
        }

        return $dom->saveXML();
    }

    private function getStyle($style = null) {
        switch($style) {
            default:
                $file = 'default.xml';
        }

        return file_get_contents('module/AFE/view/afe/kml/' . $file);
    }
}
