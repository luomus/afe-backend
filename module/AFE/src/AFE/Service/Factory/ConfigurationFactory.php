<?php
namespace AFE\Service\Factory;

use RuntimeException;
use AFE\Options\AfeConfig;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ConfigurationFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $options = $this->getOptions($serviceLocator);
        $config  = new AfeConfig($options);

        return $config;
    }

    /**
     * @param  ServiceLocatorInterface $serviceLocator
     * @return mixed
     * @throws RuntimeException
     */
    public function getOptions(ServiceLocatorInterface $serviceLocator)
    {
        $options = $serviceLocator->get('Config');
        $options = isset($options['afe']) ? $options['afe'] : null;

        if (null === $options) {
            throw new RuntimeException(
                'Configuration for AFE is not found in config.'
            );
        }

        return $options;
    }
}
