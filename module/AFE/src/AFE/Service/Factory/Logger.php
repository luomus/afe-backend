<?php

namespace AFE\Service\Factory;

use Zend\Log\Filter\Priority;
use Zend\Log\Logger as Log;
use Zend\Log\Writer\Stream;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class Logger implements FactoryInterface {

    const LOG_LEVEL = Log::INFO;

    /**
     * Creates the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return Log
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $logger   = new Log();
        $writer   = new Stream('./data/logs/' . date('Y-m-d') . '.log');

        $logger->addWriter($writer);

        $writer->addFilter(new Priority(self::LOG_LEVEL));

        return $logger;
    }

}
