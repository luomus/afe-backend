<?php

namespace AFE\Service\Factory;


use Zend\Permissions\Acl\Acl as AccessControlList;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class Acl implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('config');
        $aclConfig = $config['acl'];

        // Add the acl rules defined in the config
        $acl  = new AccessControlList();
        foreach($aclConfig['resource'] as $resource=>$parent) {
            $acl->addResource($resource, $parent);
        }

        foreach($aclConfig['role'] as $role=>$parents) {
            $acl->addRole($role, $parents);
        }

        foreach (array('deny','allow') as $action) {
            foreach($aclConfig[$action] as $definition) {
                call_user_func_array(array($acl,$action), $definition);
            }
        }

        return $acl;
    }
} 