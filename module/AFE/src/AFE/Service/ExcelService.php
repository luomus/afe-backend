<?php

namespace AFE\Service;

class ExcelService {

    protected $file;
    protected $excel;
    protected $sheetArray = [];

    public function __construct($file = null) {
        if ($file !== null) {
            $this->setFile($file);
        }
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $file
     * @throws \Exception
     */
    public function setFile($file)
    {
        if (!file_exists($file)) {
            throw new \Exception("File '$file' doesn't exist!");
        }
        $this->file = $file;
    }

    public function getSheets() {
        $excel = $this->getExcel();
        $sheets = $excel->getSheetNames();

        return $sheets;
    }

    public function SheetToArray($sheet = 0) {
        if (!isset($this->sheetArray[$sheet])) {
            $sheetData = $this->getSheet($sheet);
            $maxCell = $sheetData->getHighestRowAndColumn();
            $this->sheetArray[$sheet] = $sheetData->rangeToArray('A1:' . $maxCell['column'] . $maxCell['row'], '', true, false);
        }
        return $this->sheetArray[$sheet];
    }

    public function getRows($sheet, $skipFirst = false, $max = 0) {
        $data = $this->SheetToArray($sheet);
        if ($max > 0) {
            array_splice($data, $max);
        }
        if ($skipFirst) {
            array_shift($data);
        }
        return $data;
    }

    public function getFirstRow($sheet = 0) {
        $data = $this->SheetToArray($sheet);
        if (isset($data[0])) {
            return array_filter($data[0]);
        }
        return [];
    }

    public function getDistinctValues($sheet = 0,$max = 0) {
        $data = $this->SheetToArray($sheet);
        $dist = [];
        $cnt = [];
        foreach ($data as $rowNum => $row) {
            foreach ($row as $key => $value) {
                $value = trim($value);
                if (!empty($value)) {
                    if ($max > 0 && (!isset($dist[$key]) || !isset($dist[$key][$value]))) {
                        $cnt[$key] = isset($cnt[$key]) ? $cnt[$key] + 1 : 0;
                        if ($cnt[$key] > $max) {
                            $dist[$key]['...'] = 1;
                            break;
                        }
                    }
                    $dist[$key][$value] = 1;
                }
            }
        }
        foreach ($dist as $key => $value) {
            $dist[$key] = array_keys($dist[$key]);
        }
        return $dist;
    }

    /**
     * @param $sheet
     * @return \PHPExcel_Worksheet
     * @throws \Exception
     * @throws \PHPExcel_Exception
     */
    protected function getSheet($sheet) {
        $sheets = $this->getSheets();
        if (!is_numeric($sheet)) {
            $sheetIdx = array_search($sheet, $sheets);
            if ($sheetIdx === false) {
                throw new \Exception("Excel doesn't have sheet named '$sheet'");
            }
            $sheet = $sheetIdx;
        }
        if (!isset($sheets[$sheet])) {
            throw new \Exception("Excel doesn't have sheet '$sheet'");
        }
        return $this->getExcel()->getSheet($sheet);
    }

    protected function getExcel() {
        if ($this->excel === null) {
            $this->excel = \PHPExcel_IOFactory::load($this->file);
        }
        return $this->excel;
    }

}
