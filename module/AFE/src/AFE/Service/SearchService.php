<?php

namespace AFE\Service;

use AFE\Db\TableGateway\GridGateway;
use AFE\Model\SearchResult;

class SearchService {
    const MIN_QUERY_LENGTH = 4;
    const MAX_RESULTS = 10;
    const TIMEOUT = 10;

    const SEARCH_QUADRAT = 'quadrat';
    const SEARCH_YKJ = 'ykj';
    const SEARCH_WGS84 = 'wgs84';

    protected $searchPatterns = [
        self::SEARCH_QUADRAT => '/^([A-Za-z]+:\s)?(?P<quadrat>[A-Za-z0-9]{6})$/',
        self::SEARCH_YKJ => '/^(?P<lat>[67]{1}[0-9]{2,6})[\s:]{1}(?P<lng>[3]?[0-9]{2,6})$/',
        self::SEARCH_WGS84 => '/^(?<lat>(-?(90|(\d|[1-8]\d)([\.,]{0,1}\d{1,9}){0,1})))[\s:\,]{1}(?<lng>(-?(180|(\d|\d\d|1[0-7]\d)([\.,]{0,1}\d{1,9}){0,1})))$/',
    ];

    public function search($query) {
        if (strlen($query) < self::MIN_QUERY_LENGTH) {
            return [];
        }
        $results = [];
        foreach ($this->searchPatterns as $key => $pattern) {
            if (preg_match($pattern, $query, $match)) {
                $key = ucfirst($key);
                $checkMethod = 'check' . $key;
                if (method_exists($this, $checkMethod) && !$this->checkQuadrat($match)) {
                    continue;
                }
                $resultMethod = 'query' . $key;
                $resultPart   = $this->$resultMethod($match);
                $results = array_merge($results, $resultPart);
            }
        }
        if (!count($results)) {
            $results = $this->queryGeocode($query);
        }
        return $results;
    }

    protected function queryGeocode($query) {
        $geoCoder = new \GoogleMapsGeocoder();
        $geoCoder->setAddress($query);
        try {
            $data = $geoCoder->geocode();
        } catch (\Exception $ex) {
            return [];
        }
        if (!isset($data['status']) || $data['status'] != 'OK' || !isset($data['results'])) {
            return [];
        }
        $results = [];
        foreach ($data['results'] as $result) {
            if (!isset($result['formatted_address']) || !isset($result['geometry']) || !isset($result['geometry']['location']) || !isset($result['address_components'])) {
                continue;
            }
            $searchResult = new SearchResult();
            $searchResult->setName($result['formatted_address']);
            $searchResult->setLocation([$result['geometry']['location']]);
            foreach ($result['address_components'] as $address) {
                if (isset($address['types']) && in_array('country', $address['types'])) {
                    //TODO: if mapping to territory is wanted then mapping between country codes that google uses and the regions should be made
                    //$searchResult->setTerritory($address['short_name']);
                }
            }
            if (isset($result['geometry']['bounds'])) {
                $bounds = [];
                $bounds[] = $result['geometry']['bounds']['northeast'];
                $bounds[] = $result['geometry']['bounds']['southwest'];
                $searchResult->setBounds($bounds);
            }
            $results[] = $searchResult;
        }
        return $results ;
    }

    protected function queryQuadrat($match) {
        $quadrat     = strtoupper($match['quadrat']);
        $gridGateway = new GridGateway();
        $grids = $gridGateway->select(['QUADRAT' => $quadrat]);
        $results = [];
        foreach ($grids as $grid) {
            /** @var \AFE\Model\Grid $grid */
            $searchResult = new SearchResult();
            $searchResult->setName($grid->getTerritory() . ': ' . $grid->getQuadrat());
            $searchResult->setQuadrat($grid->getQuadrat());
            $searchResult->setTerritory($grid->getTerritory());
            $results[] = $searchResult;
        }

        return $results;
    }

    protected function queryWgs84($match) {
        $lat = str_replace(',','.', $match['lat']);
        $lng = str_replace(',','.', $match['lng']);
        $searchResult = new SearchResult();
        $searchResult->setName('wgs84: ' . $lat . ' ' . $lng);
        $searchResult->setLocation([['lat' => $lat, 'lng' => $lng]]);

        return [$searchResult];
    }

    protected function queryYkj($match) {
        $lat = $match['lat'];
        $lng = $match['lng'];

        return [];
    }

    protected function checkYkj($match) {
        $latLen = strlen($match['lat']);
        $lngLen = strlen($match['lng']);
        if ($latLen === $lngLen || $latLen === ($lngLen - 1)) {
            return true;
        }
        return false;
    }

}
