<?php
namespace AFE\Service;

use AFE\Authentication\Adapter\AfeUserAdapter;
use AFE\Db\TableGateway\UserGateway;
use AFE\Model\User;
use Zend\Math\Rand;
use Zend\Session\Container;

class UserService {

    public function getAll() {
        $userGateway = new UserGateway();

        return $userGateway->select();
    }

    public function getAllEditors() {
        $userGateway = new UserGateway();

        $users = $userGateway->select(['ROLE' => [User::ROLE_ADMIN, User::ROLE_EDITOR]]);
        $result = [];
        foreach($users as $user) {
            /** @var $user User */
            $result[] = $user->getUserID();
        }
        return $result;
    }

    public function getById($id) {
        $userGateway = new UserGateway();

        return $userGateway->select(['USERID' => $id])->current();
    }

    /**
     * @param $username
     * @return null|User
     */
    public function getByUsername($username) {
        $userGateway = new UserGateway();

        return $userGateway->select(['USERNAME' => $username])->current();
    }

    public function deleteUser(User $user) {
        $userGateway = new UserGateway();

        return $userGateway->delete(['USERID' => $user->getUserID()]);
    }

    public function addUser(User $user, $password = null) {
        if ($password === null) {
            $password = Rand::getString(16);
        }
        $data = $user->getArrayCopy(true);
        if ($user->getUserID() !== null) {
            throw new \Exception("Add user is should not be used with existing user!");
        }
        $this->setDataPassword($password, $data);

        $userGateway = new UserGateway();
        $result = $userGateway->insert($data);
        if ($result) {
            //TODO send email
        }
        return $result;
    }

    public function resetPassword(User $user) {
        $user->setToken($this->generateToken());
        //TODO sedn email
        return $this->updateUser($user);
    }

    public function updateUser(User $user, $password = null) {
        if ($user->getUserID() === null) {
            throw new \Exception("Cannot update user without id!");
        }
        $userGateway = new UserGateway();
        $data = $user->getArrayCopy(true);
        $this->setDataPassword($password, $data);

        return $userGateway->update($data, ['USERID' => $user->getUserID()]);
    }

    private function setDataPassword($password, &$data) {
        if ($password === null) {
            unset($data['PASSWD']);
        } else {
            $data['PASSWD'] = AfeUserAdapter::generateHash($password);
            $this->getTokenContainer()->offsetUnset('token');
        }
    }

    public function getUserToken($user) {
        if (!$user instanceof User) {
            return '';
        }
        $container = $this->getTokenContainer();
        $token     = $user->getToken();
        if ($token !== null) {
            $container->offsetSet('token', $token);
        } else {
            $token = $container->offsetGet('token');
        }
        return $token;
    }

    private function getTokenContainer() {
        return new Container('userToken');
    }

    private function generateToken() {
        return bin2hex(openssl_random_pseudo_bytes(32));
    }

}
