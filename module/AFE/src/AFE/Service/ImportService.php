<?php

namespace AFE\Service;

use AFE\Db\Table\RecordTable;
use AFE\Db\Table\TerritoryTable;
use AFE\Model\Record;
use AFE\InputFilter\RecordFilter;
use AFE\Query\Criteria;
use Zend\Session\Container;

class ImportService {

    const ACTION_ANALYZE  = 'analyze';
    const ACTION_VALIDATE = 'validate';
    const ACTION_SAVE     = 'save';
    const ACTION_SUCCESS  = 'success';

    const CONCAT = "\n";

    const MAX_SUCCESS = 10;

    /** @var ExcelService */
    protected $excelService;
    protected $recordForm;

    protected $skipFirst = true;
    protected $defaultSheet = 0;
    protected $defaultUserID = null;
    protected $afeFileFields = [
        'taxon',
        'quadratID',
        'statusID',
        'region',
        'ignore',
        'ignore',
        'notes',
        'accuracy_location',
        'accuracy_taxon',
    ];

    protected $typeMap = [
        'territory' => 'region',
        'territoryID' => 'regionID'
    ];

    public function __construct($file) {
        $this->excelService = new ExcelService($file);
    }

    /**
     * @return int
     */
    public function getDefaultUserID()
    {
        return $this->defaultUserID;
    }

    /**
     * @param int $defaultUserID
     */
    public function setDefaultUserID($defaultUserID)
    {
        $this->defaultUserID = $defaultUserID;
    }

    /**
     * @return boolean
     */
    public function isSkipFirst()
    {
        return $this->skipFirst;
    }

    /**
     * @param boolean $skipFirst
     */
    public function setSkipFirst($skipFirst)
    {
        $this->skipFirst = ($skipFirst === 'true' || $skipFirst === true);
    }

    /**
     * @return int
     */
    public function getDefaultSheet()
    {
        return $this->defaultSheet;
    }

    /**
     * @param int $defaultSheet
     */
    public function setDefaultSheet($defaultSheet)
    {
        $this->defaultSheet = $defaultSheet;
    }

    public function analyze($sheet = null, $skipFirstRow = null, $dataLimit = 0) {
        if ($sheet === null) {
            $sheet = $this->defaultSheet;
        }
        if ($skipFirstRow === null) {
            $skipFirstRow = $this->skipFirst;
        }
        $session  = $this->getSessionContainer();
        $session->records = null;
        $firstRow = $this->excelService->getFirstRow($sheet);
        $taxon = null;
        $analyzed = [];
        $analyzed['data']   = $this->excelService->getRows($sheet, $skipFirstRow, $dataLimit);
        if (count($firstRow) == 1) {
            $taxon = array_pop($firstRow);
            $analyzed['data'] = $this->convertFromAfeFile($analyzed['data'], $taxon);
            $firstRow = $this->afeFileFields;
        }
        $analyzed['sheets']        = $this->excelService->getSheets();
        $analyzed['skipFirstRow']  = $skipFirstRow;
        $analyzed['matchedFields'] = false;
        $fields   = \AFE\Form\Record::getFields(true);
        foreach($firstRow as $col => $type) {
            $analyzed['field'][$col]['type'] = '';
            $type = isset($this->typeMap[$type]) ? $this->typeMap[$type] : $type;
            if (in_array($type, $fields)) {
                $analyzed['matchedFields'] = true;
                $analyzed['field'][$col]['type'] = $type;
            }
        }
        $analyzed['action'] = self::ACTION_VALIDATE;
        return $analyzed;
    }

    protected function convertFromAfeFile($data, $taxon) {
        $result = [];
        foreach($data as $row) {
            if (count($row) < 2) {
                continue;
            }
            $result[] = [
                $taxon,  // taxon
                $row[0], // qaudratID
                $row[1], // statusID
                $row[3], // territory -> notes
                $row[5], // users name -> notes
                $row[6], // edit time -> notes
                (string)$row[7], // notes
                (int)!($row[2] == 1 || $row[2] == 1), // accuracy taxon
                (int)!($row[4] == 1 || $row[2] > 3), // accuracy location
            ];
        }
        return $result;
    }

    public function isAfeFileFields($fields) {
        foreach($this->afeFileFields as $key => $field) {
            if ($fields === 'ignore') {
                continue;
            }
            if (isset($fields[$key]) && $fields[$key] !== $field) {
                return false;
            }
        }
        return true;
    }

    public function validate($analyzed, $sheet = null) {
        if ($sheet === null) {
            $sheet = $this->defaultSheet;
        }
        $fields       = $this->getMappedFields($analyzed);
        $skipFirst    = isset($analyzed['skipFirstRow']) ? $analyzed['skipFirstRow'] : $this->skipFirst;
        $defaultTaxon = isset($analyzed['default']) && isset($analyzed['default']['taxonID']) ?
            $analyzed['default']['taxonID'] : null;

        $data = $this->excelService->SheetToArray($sheet);
        $taxon = '';
        $useAfeFields = $this->isAfeFileFields($fields);
        if ($useAfeFields) {
            $taxon = array_shift($data)[0];
            $data = $this->convertFromAfeFile($data, $taxon);
        }
        if ($skipFirst) {
            array_shift($data);
        }
        $session     = $this->getSessionContainer();
        $hasErrors   = false;
        $records     = [];
        $errors      = [];
        $inputFilter = new RecordFilter();
        $idx = 0;
        $analyzed['data'] = [];
        foreach($data as $num => $row) {
            $record = [];
            foreach ($fields as $col => $key) {
                if (!isset($row[$col])) {
                    continue;
                }
                $record[$key] = isset($record[$key]) ?
                    $record[$key] . self::CONCAT . $row[$col] : $row[$col];
            }
            if ($defaultTaxon && !isset($record['taxon']) && !isset($record['taxonID'])) {
                $record['taxonID'] = $defaultTaxon;
            }
            if ($this->defaultUserID) {
                $record['userID'] = $this->defaultUserID;
            }
            $inputFilter->setData($record);
            if (!$inputFilter->isValid()) {
                $errors[$idx] = $inputFilter->getMessages();
                $analyzed['data'][$idx] = $row;
                $idx++;
                $hasErrors = true;
                continue;
            }
            $model = new Record();
            $model->exchangeArray($inputFilter->getValues());
            $records[] = $model;
        }
        if (!$hasErrors) {
            $analyzed['data'] = $this->excelService->getRows($sheet,10);
            if ($useAfeFields) {
                $analyzed['data'] = $this->convertFromAfeFile($analyzed['data'], $taxon);
            }
            $session->records = $records;
            $analyzed['action'] = self::ACTION_SAVE;
            $analyzed['message'] = 'Data is ready to be stored. Below is the first 10 rows';
            if (isset($analyzed['errors'])) {
                unset($analyzed['errors']);
            }
        } else {
            $analyzed['message'] = 'Following errors where found. Please correct them and try again.';
            $analyzed['errors'] = $errors;
            $analyzed['action'] = self::ACTION_VALIDATE;
        }

        return $analyzed;
    }

    public function store($data) {
        $records = $this->getSessionContainer()->records;
        $success = true;
        $recordTable = new RecordTable();
        $criteria = new Criteria();
        foreach ($records as $record) {
            /** @var Record $record */
            $results = $recordTable->fetchBy($criteria->setCriteriaFromArray([
                'quadrat' => $record->getQuadratID(),
                'taxon' => $record->getTaxonID(),
                'userID' => $record->getUserID()
            ]));
            try {
                foreach($results as $result) {
                    $record->setRecordID($result->getRecordID());
                    break;
                }
                $recordTable->save($record);
            } catch(\Exception $e) {
                $success = false;
            }
        }
        if ($success) {
            $data['action'] = self::ACTION_SUCCESS;
        } else {
            $data['action'] = self::ACTION_SAVE;
            $data['message'] = 'Failed to safe data! Try again little later. If the error still exists please contact the AFE online support';
        }

        return $data;
    }

    private function getSessionContainer() {
        return new Container('importRecords');
    }

    /**
     * @return ExcelService
     */
    public function getExcelService()
    {
        return $this->excelService;
    }

    private function getMappedFields($analyzed) {
        if (!isset($analyzed['field'])) {
            throw new \Exception('Mapping is missing');
        }
        $fields = $analyzed['field'];
        $result = [];
        foreach($fields as $col => $data) {
            if (isset($data['type']) && !empty($data['type'])) {
                $result[$col] = $data['type'];
            }
        }
        return $result;
    }
}
