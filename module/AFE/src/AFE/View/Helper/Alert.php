<?php

namespace AFE\View\Helper;


use AFE\Stdlib\ArrayUtils;
use Zend\Escaper\Escaper;
use Zend\Form\Form;
use Zend\View\Helper\AbstractHtmlElement;


class Alert extends AbstractHtmlElement
{
    const TYPE_ERROR = 'danger';
    const TYPE_SUCCESS = 'success';

    //private $template = '<div>%location% => %msg%</div>';
    private $template = '<div>%msg%</div>';
    private $title = '';

    public function __invoke($msg = null, $type = self::TYPE_ERROR, $allowHtml = false,  $closeButton = true)
    {
        if ($msg === null) {
            return $this;
        }
        $attrs = [
            'class' => 'alert alert-' . $type
        ];
        $attrs = $this->htmlAttribs($attrs);

        if ($msg instanceof Form) {
            $messages = $msg->getMessages();
            if (empty($messages)) {
                return '';
            }
            $messages = ArrayUtils::array_to_paths($messages);
            $msg      = $this->title;
            $hasLocation = strstr($this->template, '%location%') !== false;
            foreach ($messages as $location => $error) {
                $line = $this->template;
                $line = str_replace('%msg%', (string) $error, $line);
                if ($hasLocation) {
                    $line = str_replace('%location%', (string) $location, $line);
                }
                $msg .= $line;
            }
        } elseif (is_string($msg)) {
            if (!$allowHtml) {
                $escape = new Escaper('utf-8');
                $msg = $escape->escapeHtml($msg);
            }
        } else {
            return '';
        }

        $html = '<div ' . $attrs . '>' . $msg . '</div>';

        return $html;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function setTemplate($template) {
        if (strstr($this->template, '%msg%') === false) {
            throw new \Exception('Message spot is missing from the alert template');
        }
        $this->template = $template;
    }

    public function __toString() {
        return '';
    }

} 