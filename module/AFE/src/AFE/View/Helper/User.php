<?php

namespace AFE\View\Helper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\View\Helper\AbstractHtmlElement;


class User extends AbstractHtmlElement implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    const TYPE_FULLNAME = 'fullname';

    public function __invoke($type = self::TYPE_FULLNAME)
    {
        $auth = $this->serviceLocator->getServiceLocator()->get('auth');
        if (!$auth->hasIdentity()) {
            return 'guest';
        }
        /** @var \AFE\Model\User $user */
        $user = $auth->getIdentity();
        switch($type) {
            case self::TYPE_FULLNAME:
                return $user->getLastname() . ', ' . $user->getFirstname();
        }
        return '';
    }

} 