<?php

namespace AFE\Filter;


use Zend\Filter\AbstractFilter;
use Zend\Filter\FilterChain;
use Zend\Filter\FilterInterface;

class RecordFilter extends AbstractFilter {

    const ALL_FIELDS = '_all_';

    protected $fields = [
        'status'
    ];

    protected $filterChain = [];

    public function attach(FilterInterface $filter, $fields = self::ALL_FIELDS, $priority = FilterChain::DEFAULT_PRIORITY)
    {
        if (!isset($this->filterChain[$fields])) {
            $this->filterChain[$fields] = new FilterChain();
        }
        $this->filterChain[$fields]->attach($filter, $priority);

        return $this;
    }

    /**
     * Filters the record array
     *
     * @param  mixed $value
     * @return mixed
     */
    public function filter($value)
    {
        if (!is_array($value)) {
            return $value;
        }
        $filteredValue = $value;
        foreach ($this->filterChain as $field => $filter) {
            /** @var FilterChain $filter */
            if ($field === self::ALL_FIELDS) {
                $filteredValue = $filter->filter($filteredValue);
            } elseif (isset($filteredValue[$field])) {
                $filteredValue[$field] = $filter->filter($filteredValue[$field]);
            }
        }

        return $filteredValue;
    }

}
