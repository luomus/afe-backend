<?php

namespace AFE\Filter;


use AFE\Db\Table\StatusTable;
use AFE\Model\Status;
use Zend\Filter\AbstractFilter;
use Zend\Filter\Exception;

class StatusToStatusID  extends AbstractFilter{

    public static $stats;

    public function __construct() {
        if (self::$stats === null) {
            $this->initStatus();
        }
    }

    /**
     * Returns the result of filtering $value
     *
     * @param  mixed $value
     * @throws Exception\RuntimeException If filtering $value is impossible
     * @return mixed
     */
    public function filter($value)
    {
        if (!is_array($value) || !isset($value['status'])) {
            return $value;
        }
        $id = isset(self::$stats[$value['status']]) ? self::$stats[$value['status']] : $value['status'];
        $value['statusID'] = $id;
        return $value;
    }

    private function initStatus() {
        $statusTable = new StatusTable();
        $results = $statusTable->fetchAll();
        $data = [];
        foreach($results as $status) {
            /** @var Status $status */
            $data[$status->getStatusText()] = $status->getStatusID();
        }
        self::$stats = $data;
    }
}
