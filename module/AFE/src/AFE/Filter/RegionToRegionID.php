<?php

namespace AFE\Filter;


use AFE\Db\Table\StatusTable;
use AFE\Db\Table\TerritoryTable;
use AFE\Model\Status;
use Zend\Filter\AbstractFilter;
use Zend\Filter\Exception;

class RegionToRegionID extends AbstractFilter{

    public static $stats;

    public function __construct() {
        if (self::$stats === null) {
            $this->initRegions();
        }
    }

    /**
     * Returns the result of filtering $value
     *
     * @param  mixed $value
     * @throws Exception\RuntimeException If filtering $value is impossible
     * @return mixed
     */
    public function filter($value)
    {
        if (!is_array($value) || !isset($value['region'])) {
            return $value;
        }
        $value['regionID'] = isset(self::$stats[$value['region']]) ? self::$stats[$value['region']] : $value['region'];
        return $value;
    }

    private function initRegions() {
        $territoryTable = new TerritoryTable();
        $areas = $territoryTable->fetchAll();
        $data = [];

        foreach ($areas as $area) {
            /** @var $area \AFE\Model\Territory */
            $data[$area->getTerritory()] =$area->getTerritoryID();
            $data[$area->getName()] = $area->getTerritoryID();
        }
        self::$stats = $data;
    }
}
