<?php

namespace AFE\Filter;


use AFE\Db\Table\TaxonTable;
use AFE\Model\Taxon;
use Zend\Filter\AbstractFilter;
use Zend\Filter\Exception;

class TaxonToTaxonID  extends AbstractFilter{

    public static $taxa;

    public function __construct() {
        if (self::$taxa === null) {
            $this->initTaxa();
        }
    }

    /**
     * Returns the result of filtering $value
     *
     * @param  mixed $value
     * @throws Exception\RuntimeException If filtering $value is impossible
     * @return mixed
     */
    public function filter($value)
    {
        if (!is_array($value) || !isset($value['taxon'])) {
            return $value;
        }
        $id = isset(self::$taxa[$value['taxon']]) ? self::$taxa[$value['taxon']] : $value['taxon'];
        $value['taxonID'] = $id;
        return $value;
    }

    private function initTaxa() {
        $taxonTable = new TaxonTable();
        $results = $taxonTable->fetchAll();
        $data = [];
        foreach($results as $taxon) {
            /** @var Taxon $taxon */
            $data[$taxon->getTaxonName()] = $taxon->getTaxonID();
        }
        self::$taxa = $data;
    }
}
