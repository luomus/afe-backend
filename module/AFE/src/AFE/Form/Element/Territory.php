<?php

namespace AFE\Form\Element;

use Zend\Form\Element\Select;

class Territory extends Select {

    public function init() {
        $this->setValueOptions(array(
            'finland' => 'Finland',
            'sweden' => 'Sweden',
            'norway' => 'Norway',
        ));
    }

} 