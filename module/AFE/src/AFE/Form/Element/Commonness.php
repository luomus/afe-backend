<?php
namespace AFE\Form\Element;

use Zend\Form\Element\Select;

class Commonness extends Select {

    public function init() {
        $this->setValueOptions(array(
            '-1' => 'no data',
            '0' => '0 - absent',
            '1' => '1 - extremely rare',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
            '6' => '6',
            '7' => '7',
            '8' => '8',
            '9' => '9',
            '10' => '10 - extremely common',
        ));
    }

} 