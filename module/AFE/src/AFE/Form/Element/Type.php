<?php

namespace AFE\Form\Element;

use Zend\Form\Element\Select;

class Type extends Select {

    public function init() {
        $this->setValueOptions(array(
           '-1' => 'no data',
            '0' => 'absent',
            '1' => 'record(s) uncertain',
            '2' => 'extinct introduction',
            '3' => 'probably extinct intoduction',
            '4' => 'extinct native',
            '5' => 'probably extinct native',
            '6' => 'introduction',
            '7' => 'status unknown',
            '8' => 'native'
        ));
    }


} 