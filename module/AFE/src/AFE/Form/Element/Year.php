<?php

namespace AFE\Form\Element;

use Zend\Form\Element\Select;

class Year extends Select {

    const TRACK_UNTILL = 1980;

    public function init() {
        $years = [];
        $now = (int)date("Y");
        for ($i = $now; $i >= self::TRACK_UNTILL; $i--) {
            $years[$i] = $i;
        }

        $this->setValueOptions($years);
    }


} 