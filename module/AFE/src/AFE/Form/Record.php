<?php

namespace AFE\Form;

use AFE\InputFilter\RecordFilter;
use AFE\Model\Record as RecordModel;
use Zend\Form\Form;
use Zend\Stdlib\Hydrator\ArraySerializable;

class Record extends Form
{
    private static $fields = [
        'recordID' => 'hidden',
        'userID' => 'text',
        'taxonID' => 'text',
        'quadratID' => 'text',
        'regionID' => 'text',
        'statusID' => 'text',
        'accuracy_taxon' => 'checkbox',
        'accuracy_location' => 'checkbox',
        'notes' => 'textarea'
    ];

    private static $extendedFields = [
        'taxon' => 'text',
        'status' => 'text',
        'region' => 'text',
        'uncertain_taxon' => 'checkbox',
        'uncertain_location' => 'checkbox',
    ];

    public function init() {
        $this->setObject(new RecordModel);
        $this->setHydrator(new ArraySerializable());
        $this->setInputFilter(new RecordFilter());

        foreach (self::$fields as $name => $type) {
            $this->add([
                'name' => $name,
                'type' => $type
            ]);
        }
    }

    public static function getFields($includeExtended = false) {
        $fields = array_keys(self::$fields);
        if ($includeExtended) {
            $fields = array_merge($fields, array_keys(self::$extendedFields));
        }
        return $fields;
    }
}
