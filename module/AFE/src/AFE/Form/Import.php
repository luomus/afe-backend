<?php

namespace AFE\Form;

use Kotka\InputFilter\ImportExcelFilter;
use Zend\Form\Form;
use Zend\InputFilter\FileInput;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterProviderInterface;

class Import extends Form implements InputFilterProviderInterface
{
    public function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);
        $this->init();
    }


    public function init()
    {
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'file',
            'type' => 'file',
            'options' => array(
                'label' => 'file'
            ),
            'attributes' => array(
                'required' => true
            )
        ));

    }

    public function getInputFilterSpecification()
    {
        return array(
            'file' => array(
                'validators' => array(
                    array(
                        'name' => 'Zend\Validator\File\Size',
                        'options' => array(
                            'max' => '2MB'
                        )
                    ),
                    /*array(
                        'name' => 'Zend\Validator\File\MimeType',
                        'options' => array(
                            'mimeType' => 'application/pdf',
                            'messages' => array(
                                \Zend\Validator\File\MimeType::FALSE_TYPE => "Only PDF files please"
                            )
                        )
                    )*/
                )
            )
        );
    }


}
