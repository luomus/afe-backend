<?php

namespace AFE\Form;

use AFE\Model\User as UserModel;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ArraySerializable;
use Zend\Validator\NotEmpty;

class User extends Form implements InputFilterProviderInterface
{

    private $allGroups = ['password','userid','role','email','firstname','lastname'];

    public function setExistingValidationGroup(array $validationGroup = null) {
        $group = $this->getValidationGroup();
        if ($group === InputFilterInterface::VALIDATE_ALL) {
            $group = $this->allGroups;
        }
        $group = array_intersect($group, $validationGroup);
        parent::setValidationGroup($group);
    }

    public function setAddValidationGroup() {
        $this->setValidationGroup(['role','username','email','firstname','lastname']);
    }

    public function setEditValidationGroup() {
        $this->setValidationGroup(['password','userid','role','email','firstname','lastname']);
    }

    public function setLoginValidationGroup() {
        $this->setValidationGroup(['username','password']);
    }

    public function setPWResetValidationGroup() {
        $this->setValidationGroup(['username','password','password-confirmation']);
    }

    public function setEmailValidationGroup() {
        $this->setValidationGroup(['email']);
    }

    public function init() {
        $this->setObject(new UserModel());
        $this->setHydrator(new ArraySerializable());

        $this->add([
            'name' => 'userid',
            'type' => 'hidden',
        ]);

        $this->add([
            'name' => 'role',
            'type' => 'select',
            'attributes' => [
                'id' => 'user-role'
            ],
            'options' => [
                'label' => 'Role',
                'column-size' => 'sm-12',
                'value_options' => [
                    UserModel::ROLE_MEMBER => 'Member',
                    UserModel::ROLE_COLLABORATOR => 'Collaborator',
                    UserModel::ROLE_EDITOR => 'Editor',
                    UserModel::ROLE_ADMIN => 'Admin (allowed to do everything!)'
                ]
            ]
        ]);

        $this->add([
            'name' => 'username',
            'type' => 'text',
            'attributes' => array(
                'placeholder' => 'Username'
            )
        ]);

        $this->add([
            'name' => 'password',
            'type' => 'password',
            'attributes' => array(
                'placeholder' => 'Password'
            )
        ]);

        $this->add([
            'name' => 'password-confirmation',
            'type' => 'password',
            'attributes' => array(
                'placeholder' => 'Password again'
            )
        ]);

        $this->add([
            'name' => 'remember',
            'type' => 'checkbox',
            'attributes' => array(
                'class' => 'afe',
            )
        ]);

        $this->add([
            'name' => 'email',
            'type' => 'email',
            'attributes' => [
                'class' => 'user-email'
            ],
            'options' => [
                'label' => 'Email',
                'column-size' => 'sm-12'
            ]
        ]);

        $this->add([
            'name' => 'firstname',
            'type' => 'text',
            'attributes' => [
                'placeholder' => 'Firstname',
                'class' => 'user-firstname'
            ],
            'options' => [
                'label' => 'Firstname',
                'column-size' => 'sm-6'
            ]
        ]);

        $this->add([
            'name' => 'lastname',
            'type' => 'text',
            'attributes' => [
                'placeholder' => 'Lastname'
            ],
            'options' => [
                'label' => 'Lastname',
                'column-size' => 'sm-6'
            ]
        ]);

        $this->add([
            'name' => 'territories',
            'type' => 'select',
            'attributes' => [
                'multiple' => true,
                'class' => 'territories-select tag-style-default',
                'data-placeholder' => 'select'
            ],
            'options' => [
                'label' => 'Rights to these territories',
                'column-size' => 'sm-12',
                'value_options' => [
                    'fi' => 'Finland',
                    'sv' => 'Sweden',
                    'no' => 'Norway',
                    'dk' => 'Denmark',
                    'is' => 'Island',
                    'uk' => 'Great Britain',
                    'ge' => 'Germany',
                ]
            ]
        ]);

    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array(
            'username' => array(
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'notEmpty',
                        'options' => array(
                            'messages' => array(
                                NotEmpty::IS_EMPTY => 'Username is required'
                            )
                        )
                    )
                )
            ),
            'password' => array(
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'notEmpty',
                        'options' => array(
                            'messages' => array(
                                NotEmpty::IS_EMPTY => 'Password is required'
                            )
                        )
                    )
                )
            ),
            'email' => array(
                'required' => true
            ),
        );
    }
}
