<?php
namespace AFE\Form;

use Zend\Form\Form;

class MapTool extends Form {

    public function init() {

        $this->setAttribute('class', 'form-group-sm form-horizontal height-max');

        $this->add(array(
            'name' => 'territory',
            'type' => 'AFE\Form\Element\Territory',
            'options' => array(
                'label' => 'Territory',
                'label_attributes' => array(
                    'class'  => 'control-label'
                ),
            ),
            'attributes' => array(
                'id' => 'territory',
                'class' => 'form-control'
            )
        ));

        $this->add(array(
            'name' => 'type',
            'type' => 'AFE\Form\Element\Type',
            'options' => array(
                'label' => 'Status',
                'label_attributes' => array(
                    'class'  => 'control-label'
                ),
            ),
            'attributes' => array(
                'id' => 'type',
                'class' => 'form-control'
            )
        ));

        $this->add(array(
            'name' => 'commonness',
            'type' => 'AFE\Form\Element\Commonness',
            'options' => array(
                'label' => 'Commonness',
                'label_attributes' => array(
                    'class'  => 'control-label'
                ),
            ),
            'attributes' => array(
                'id' => 'commonness',
                'class' => 'form-control'
            )
        ));

        $this->add(array(
            'name' => 'commonness',
            'type' => 'AFE\Form\Element\Year',
            'options' => array(
                'label' => 'Year',
                'label_attributes' => array(
                    'class'  => 'control-label'
                ),
            ),
            'attributes' => array(
                'id' => 'year',
                'class' => 'form-control'
            )
        ));

        $this->add(array(
            'name' => 'recorn_cnt',
            'type' => 'text',
            'options' => array(
                'label' => 'Record count',
                'label_attributes' => array(
                    'class'  => 'control-label'
                ),
            ),
            'attributes' => array(
                'id' => 'count',
                'class' => 'form-control'
            )
        ));

        $this->add(array(
            'name' => 'notes',
            'type' => 'textarea',
            'options' => array(
                'label' => 'Notes',
                'label_attributes' => array(
                    'class'  => 'control-label'
                ),
            ),
            'attributes' => array(
                'id' => 'notes',
                'class' => 'form-control',
                'style' => 'resize: none;'
            )
        ));

    }

} 