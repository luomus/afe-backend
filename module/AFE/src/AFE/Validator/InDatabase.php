<?php
/**
 * Created by PhpStorm.
 * User: Localadmin_vixriihi
 * Date: 29.5.2015
 * Time: 16:45
 */

namespace AFE\Validator;


use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;
use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;

class InDatabase extends AbstractValidator
{
    const NOT_FOUND = 'notFound';
    
    protected static $cache = [];
    
    protected $table;
    protected $column;
    protected $distinct = false;

    /**
     * @var array
     */
    protected $messageTemplates = array(
        self::NOT_FOUND => "Couldn't find value for %value%!",
    );

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @param string $table
     */
    public function setTable($table)
    {
        $this->table = $table;
    }

    /**
     * @return string
     */
    public function getColumn()
    {
        return $this->column;
    }

    /**
     * @param string $column
     */
    public function setColumn($column)
    {
        $this->column = $column;
    }

    /**
     * @return boolean
     */
    public function isDistinct()
    {
        return $this->distinct;
    }

    /**
     * @param boolean $distinct
     */
    public function setDistinct($distinct)
    {
        $this->distinct = (bool)$distinct;
    }
       

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  string $value
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid($value)
    {
        $table = $this->getTable();
        $column = $this->getColumn();
        $distinct = $this->isDistinct();

        if (empty($table) || empty($column)) {
            throw new Exception\RuntimeException('Missing table or column information from the validator');
        }

        $data = $this->getData($table, $column, $distinct);
        if (isset($data[$value])) {
            return true;
        }
        $this->setValue($value);
        $this->error(self::NOT_FOUND);

        return false;
    }

    private function getData($table, $column, $distinct)
    {
        if (!isset(self::$cache[$table])) {
            $adapter = GlobalAdapterFeature::getStaticAdapter();
            $sql = new Sql($adapter);
            $select = $sql->select($table);
            $select->columns([$column]);
            if ($distinct) {
                $select->quantifier('DISTINCT');
            }
            $stmt = $sql->prepareStatementForSqlObject($select);
            $results = $stmt->execute();
            $values = [];
            foreach($results as $result) {
                $values[$result[$column]] = true;
            }
            self::$cache[$table] = $values;
        }
        return self::$cache[$table];
    }


}