<?php

namespace AFE\Navigation;


use Triplestore\Service\ObjectManager;
use Zend\Cache\Storage\StorageInterface;
use Zend\Mvc\MvcEvent;
use Zend\Navigation\Service\DefaultNavigationFactory;
use Zend\ServiceManager\ServiceLocatorInterface;

class Navigation extends DefaultNavigationFactory {

    const NAV_CACHE = 'navigation_cache';

    protected function getPages(ServiceLocatorInterface $serviceLocator)
    {
        if ($this->pages === null) {
            /** @var StorageInterface $cache */
            $cache = $serviceLocator->get('cache');
            if (!$cache->hasItem(self::NAV_CACHE)) {
                $config = $serviceLocator->get('config');
                $navigation = $config['navigation'];
                $cache->setItem(self::NAV_CACHE, $navigation);
            }

            /** @var MvcEvent $mvcEvent */
            $mvcEvent = $serviceLocator->get('Application')->getMvcEvent();
            $routeMatch = $mvcEvent->getRouteMatch();
            $router  = $mvcEvent->getRouter();
            $pages   = $this->getPagesFromConfig($cache->getItem(self::NAV_CACHE));
            $this->pages = $this->injectComponents(
                $pages,
                $routeMatch,
                $router
            );
        }
        return $this->pages;
    }
} 