<?php

namespace AFE\Options;

use Zend\Stdlib\AbstractOptions;

class AfeConfig extends AbstractOptions {

    /**
     * ID number of the book volume that is currently under construction
     *
     * @var int
     */
    protected $volumeID = 1;

    /**
     * @return int
     */
    public function getVolumeID()
    {
        return $this->volumeID;
    }

    /**
     * @param int $volumeID
     */
    public function setVolumeID($volumeID)
    {
        $this->volumeID = $volumeID;
    }



}
