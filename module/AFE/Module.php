<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonAFE for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace AFE;

use AFE\Model\User;
use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;
use Zend\EventManager\EventManager;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Permissions\Acl\Exception\ExceptionInterface as AclException;
use Zend\View\Helper\Navigation\AbstractHelper as NavigationHelper;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $eventManager->attach(MvcEvent::EVENT_ROUTE, array($this, 'protectPage'), -100);
        $adapter = $e->getApplication()->getServiceManager()->get('default');
        GlobalAdapterFeature::setStaticAdapter($adapter);
    }

    public function protectPage(MvcEvent $event)
    {
        $match = $event->getRouteMatch();
        if(!$match) {
            // we cannot do anything without a resolved route
            return;
        }
        $controller = $match->getParam('controller');
        $action     = $match->getParam('action');
        $namespace  = $match->getParam('__NAMESPACE__');

        if ($namespace === null) {
            $parts           = explode('\\', $controller);
            $moduleNamespace = $parts[0];
        } else {
            $parts           = explode('\\', $namespace);
            $moduleNamespace = $parts[0];
        }

        $services = $event->getApplication()->getServiceManager();
        $config   = $services->get('config');

        /** @var \Zend\Authentication\AuthenticationService $auth */
        $auth     = $services->get('auth');
        $acl      = $services->get('acl');
        $current  = null;
        if ($auth->hasIdentity()) {
            $current = $auth->getIdentity();
        }
        if (!$current instanceof User) {
            $role = User::DEFAULT_ROLE;
        } else {
            $role = $current->getRole();
        }

        // This is how we add default acl and role to the navigation view helpers
        NavigationHelper::setDefaultAcl($acl);
        NavigationHelper::setDefaultRole($role);

        // check if the current module wants to use the ACL
        $aclModules = $config['acl']['modules'];
        if (!empty($aclModules) && !in_array($moduleNamespace, $aclModules)) {
            return;
        }

        // Get the short name of the controller and use it as resource name
        $resourceAliases = $config['acl']['resource_aliases'];
        if (isset($resourceAliases[$controller])) {
            $resource = $resourceAliases[$controller];
        } else if (isset($resourceAliases[$moduleNamespace])) {
            $resource = $resourceAliases[$moduleNamespace];
        } else {
            $resource = strtolower(substr($controller, strrpos($controller,'\\')+1));
        }

        // If a resource is not in the ACL add it
        if(!$acl->hasResource($resource)) {
            $acl->addResource($resource);
        }
        try {
            if($acl->isAllowed($role, $resource, $action)) {
                return;
            }
        } catch(AclException $ex) {
            /** @var \Zend\Log\Logger $log */
            $log = $services->get('logger');
            $log->alert($ex->getMessage());
        }

        // If the role is not allowed access to the resource we have to redirect the
        // current user to the log in page.
        $e = new EventManager('user');
        $e->trigger('deny', $this, array(
                'match' => $match,
                'role'  => $role,
                'acl'   => $acl
            )
        );

        // Gets the router object
        $router = $event->getRouter();
        $uri = $event->getRequest()->getUri();
        $loginPage = $router->assemble(
            array('controller' => 'user', 'action' => 'login'),
            array('name' => 'default', 'query' => array('redirect' => ''.$uri))
        );

        $response = $event->getResponse();
        //$response->getHeaders()->addHeaderLine('Location', $loginPage);
        $response->setStatusCode(401);

        return $response;
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
}
