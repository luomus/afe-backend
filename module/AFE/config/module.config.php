<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonAFE for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'default' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/[:controller[/:action]]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AFE\Controller',
                        'controller'    => 'Home',
                        'action'        => 'index',
                    ),
                ),
            ),
            'kml' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/kml/[[:action/]:territory].kml',
                    'defaults' => array(
                        'controller' => 'AFE\Controller\Kml',
                        'action' => 'index',
                        'territory' => null
                    )
                )
            )
        ),
    ),
    'service_manager' => array(
        'invokables' => array(
            'AFE\Service\SearchService' => 'AFE\Service\SearchService'
        ),
        'factories' => array(
            'navigation' => 'AFE\Navigation\Navigation',
            'cache' => 'Zend\Cache\Service\StorageCacheFactory',
            'auth' => 'AFE\Authentication\AuthenticationServiceFactory',
            'acl' => 'AFE\Service\Factory\Acl',
            'logger' => 'AFE\Service\Factory\Logger',
            'afeConfig' => 'AFE\Service\Factory\ConfigurationFactory'
        ),
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
            'Zend\Db\Adapter\AdapterAbstractServiceFactory', // for production
            //'AFE\Db\Adapter\ProfilingAdapterAbstractServiceFactory' // only for debugging
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'AFE\Controller\Kml' => 'AFE\Controller\KmlController',
            'AFE\Controller\Home' => 'AFE\Controller\HomeController',
            'AFE\Controller\Marker' => 'AFE\Controller\MarkerController',
            'AFE\Controller\Editor' => 'AFE\Controller\EditorController',
            'AFE\Controller\User' => 'AFE\Controller\UserController',
            'AFE\Controller\Admin' => 'AFE\Controller\AdminController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/afe/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'view_helpers' => array(
        'invokables' => array(
            'Alert' => 'AFE\View\Helper\Alert',
            'User' => 'AFE\View\Helper\User',
        )
    ),
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
    'navigation' => array(
        'home' => array(
            'label' => 'Home',
            'icon'  => 'fa fa-home',
            'route' => 'default',
            'controller' => 'home',
            'action' => 'index',
            'resource' => 'home',
            'privilege' => 'index',
        ),
        'marker' => array(
            'label' => 'Marker tool',
            'icon'  => 'fa fa-globe',
            'route' => 'default',
            'controller' => 'marker',
            'action' => 'index',
            'resource' => 'marker',
            'privilege' => 'edit',
        ),
        'editor' => array(
            'label' => 'Editor tool',
            'icon'  => 'fa fa-edit',
            'route' => 'default',
            'controller' => 'editor',
            'action' => 'index',
            'resource' => 'editor',
            'privilege' => 'edit',
        ),
        'admin' => array(
            'separator' => true,
            'label' => 'Admin',
            'icon'  => 'fa fa-gavel',
            'route' => 'default',
            'controller' => 'edit',
            'resource' => 'admin',
            'privilege' => null,
        ),
        'admin_users' => array(
            'label' => 'Users',
            'icon'  => 'fa fa-users',
            'class' => 'briefcase',
            'route' => 'default',
            'controller' => 'admin',
            'action' => 'users',
            'resource' => 'admin',
            'privilege' => 'users',
        )
    ),

);
