<?php

namespace Console\Service;


class FormMessageService {

    public function messagesToString($messages) {
        $result = '';
        foreach ($messages as $place => $errors) {
            foreach ($errors as $error => $message) {
                $result .= $place . ': ' . $message . "\n";
            }
        }

        return $result;
    }

}
