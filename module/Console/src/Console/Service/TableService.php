<?php

namespace Console\Service;
use Zend\Stdlib\ArraySerializableInterface;
use Zend\Text\Table\Column;
use Zend\Text\Table\Row;
use Zend\Text\Table\Table;

class TableService {

    public function createTable($data, array $fields, $title = null, $padding = 0) {
        $width = [];
        $fieldsKey = array_keys($fields);
        foreach ($fieldsKey as $key => $field) {
            $width[$key] = strlen($field) + ($padding * 2);
        }
        $table = $this->getTable($title, count($fieldsKey));
        $table->setColumnWidths($width);

        // Table column headings
        $row = new Row();
        foreach ($fieldsKey as $key) {
            $row->createColumn($key, ['align' => Column::ALIGN_CENTER]);
        }
        $table->appendRow($row);

        // Table rows
        foreach ($data as $row) {
            $table->appendRow(
                $this->getTableRow($row, $fields, $width, $padding)
            );
        }
        $table->setColumnWidths($width);

        return $table;
    }

    public function createKeyValueTable($data, $title = null, $padding = 0) {
        $titleLen = strlen($title);
        $width = [$titleLen, $titleLen];
        $table = $this->getTable($title, 2, $padding);
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $valueMax = 0;
                foreach ($value as $k2 => $v2) {
                    $valueLen = strlen($v2);
                    if ($valueMax < $valueLen) {
                        $valueMax = $valueLen;
                    }
                }
                $valueLen = $valueMax;
                implode("\n", $value);
            } else {
                $valueLen = strlen($value);
            }
            $keyLen = strlen($key) + ($padding * 2);
            $valueLen = $valueLen + ($padding * 2);
            if ($value === null) {
                $value = '';
            }

            if ($keyLen > $width[0]) {
                $width[0] = $keyLen;
            }
            if ($valueLen > $width[1]) {
                $width[1] = $valueLen;
            }
            $row = new Row();
            $row->createColumn($key, ['align' => Column::ALIGN_RIGHT]);
            $row->createColumn($value);

            $table->appendRow($row);
        }
        $table->setColumnWidths($width);

        return $table;
    }

    /**
     * @param $title
     * @param int $colSpan
     * @return Table
     */
    private function getTable($title, $colSpan = 1, $padding = 0) {
        $table = new Table();

        //Table title
        if ($title !== null) {
            $row = new Row();
            $row->createColumn($title, ['align' => Column::ALIGN_CENTER, 'colSpan' => $colSpan]);
            $table->appendRow($row);
        }

        $table->setPadding($padding);

        return $table;
    }

    private function getTableRow($row, $fields, &$with, $padding = 0) {
        $result = [];
        if (is_object($row)) {
            $row = $this->extract($row);
        }
        if (!is_array($row)) {
            throw new \Exception('Data given to table service is not an array!');
        }
        $cnt = 0;
        foreach ($fields as $field => $data) {
            $value = '';
            if (is_string($data) && isset($row[$data])) {
                $value = $row[$data];
            } else if (is_array($data)) {
                $separator = ' ';
                $value = [];
                if (isset($data['separator'])) {
                    $separator = $data['separator'];
                    unset($data['separator']);
                }
                foreach ($data as $combine) {
                    if (isset($row[$combine])) {
                        $value[] = $row[$combine];
                    }
                }
                $value = implode($separator, $value);
            }
            $result[] = $value;

            // Make sure that width is always correct
            $len = strlen($value) + ($padding * 2);
            if ($len > $with[$cnt]) {
                $with[$cnt] = $len;
            }
            $cnt++;

        }

        return $result;
    }

    private function extract($object) {
        if ($object instanceof ArraySerializableInterface) {
            return $object->getArrayCopy();
        } elseif (method_exists($object, 'toArray')) {
            return $object->toArray();
        }
        return null;
    }

}
