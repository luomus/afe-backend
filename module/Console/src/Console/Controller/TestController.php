<?php

namespace Console\Controller;

use AFE\Model\User;
use AFE\Service\ExcelService;
use AFE\Service\UserService;
use Console\Service\FormMessageService;
use Console\Service\TableService;
use Zend\Console\Prompt\Confirm;
use Zend\Console\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Controller\AbstractConsoleController;

class TestController extends AbstractConsoleController
{

    public function indexAction() {
        echo "Testing tool for AFE";
    }


    public function excelAction() {
        $excelService  = new ExcelService();
        /** @var Request $request */
        $request = $this->getRequest();
        $file = $request->getParam('file');
        $excelService->setFile($file);
        $data = $excelService->getDistinctValues();
        var_dump($data);

        echo 'Done';
    }

}
