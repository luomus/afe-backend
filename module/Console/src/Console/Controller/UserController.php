<?php

namespace Console\Controller;

use AFE\Model\User;
use AFE\Service\UserService;
use Console\Service\FormMessageService;
use Console\Service\TableService;
use Zend\Console\Prompt\Confirm;
use Zend\Console\Request;
use Zend\Mvc\Controller\AbstractActionController;

/**
 * Class UserController is commandline controller that is used for user management
 * Available options can be found by running php backend.php
 *
 * @package Console\Controller
 */
class UserController extends AbstractActionController
{

    /**
     * Action that lists all the users in AFE to console
     */
    public function listAction() {
        $userService  = new UserService();
        $tableService = new TableService();
        $users = $userService->getAll();
        $options = [
            'id' => 'userID',
            'username' => 'username',
            'fullneme' => [
                'firstname',
                'lastname'
            ],
            'email' => 'email'
        ];
        $table = $tableService->createTable($users, $options, 'All users', 1);
        $table->setPadding(1);
        echo $table;
    }

    /**
     * Action that shows more information about the specific username
     */
    public function infoAction() {
        $userService  = new UserService();
        $tableService = new TableService();
        /** @var Request $request */
        $request = $this->getRequest();
        $username = $request->getParam('username');
        if ($username === null) {
            echo "You forgot to give username parameter\n";
            return;
        }
        /** @var User $user */
        $user = $userService->getByUsername($username);
        if ($user === null) {
            echo "Couldn't find '$username'1\n";
            return;
        }
        $data = $user->getArrayCopy();
        if (array_key_exists('passwd', $data)) {
            unset($data['passwd']);
        }
        if (array_key_exists('token', $data)) {
            unset($data['token']);
        }

        $table = $tableService->createKeyValueTable($data, 'USER: ' . $username, 1);
        $table->setPadding(1);
        echo $table;
    }

    /**
     * Adds new user
     *
     * If password is not given one will be generated on the fly
     * After successful addition email with password will be sent to the user
     *
     * @throws \Exception
     */
    public function addAction() {
        $userService = new UserService();
        $formService = new FormMessageService();
        /** @var Request $request */
        $request     = $this->getRequest();
        $addForm     = $this->getAddForm();
        $data        = $request->getParams();
        $password    = $request->getParam('password');
        $addForm->setData($data);
        if (!$addForm->isValid()) {
            $messages = $addForm->getMessages();
            $reason   = $formService->messagesToString($messages);
            echo "Could not add user! Error in following parameters\n";
            echo $reason;
            return;
        }
        /** @var User $user */
        $user   = $addForm->getData();
        $result = $userService->addUser($user, $password);
        if ($result) {
            echo "User " . $user->getUsername() . " added to AFE\n";
        } else {
            echo "Failed to add or send email to the user\n";
        }
    }

    /**
     * Modifies user data
     *
     * @throws \Exception
     */
    public function modAction() {
        $userService = new UserService();
        $formService = new FormMessageService();
        /** @var Request $request */
        $request     = $this->getRequest();
        $modForm     = $this->getEditForm();
        $data        = $request->getParams()->toArray();
        $username    = $request->getParam('username');
        $password    = $request->getParam('password');
        $user        = $userService->getByUsername($username);
        if ($user === null) {
            echo "Couldn't find user '$username' in AFE\n";
            return;
        }
        $modForm->bind($user);
        $modForm->setData($data);
        $modForm->setExistingValidationGroup(array_keys($data));
        if (!$modForm->isValid()) {
            $messages = $modForm->getMessages();
            $reason   = $formService->messagesToString($messages);
            echo "Could not modify user! Error in following parameters\n";
            echo $reason;
            return;
        }
        /** @var User $user */
        $user   = $modForm->getData();
        $result = $userService->updateUser($user, $password);
        if ($result) {
            echo "Modified user '" . $user->getUsername() . "'\n";
        } else {
            echo "Failed to modify user\n";
        }
    }

    /**
     * Deletes the user
     */
    public function delAction() {
        $userService = new UserService();
        /** @var Request $request */
        $request     = $this->getRequest();
        $username    = $request->getParam('username');
        $user        = $userService->getByUsername($username);
        if ($user === null) {
            echo "Couldn't find user '$username' in AFE\n";
            return;
        }
        if (Confirm::prompt("Are you sure that you want to delete user '$username'? [y/n]", 'y','n')) {
            if ($userService->deleteUser($user)) {
                echo "User '$username' deleted!\n";
            } else {
                echo "Failed to delete '$username'\n";
            }
        } else {
            echo "Canceled!\n";
        }
    }

    /**
     * Returns the form that is used for editing user data
     *
     * @return \AFE\Form\User
     */
    private function getEditForm() {
        $form = $this->getUserForm();
        $form->setEditValidationGroup();

        return $form;
    }

    /**
     * Return the form that is used for adding user
     *
     * @return \AFE\Form\User
     */
    private function getAddForm() {
        $form = $this->getUserForm();
        $form->setAddValidationGroup();

        return $form;
    }

    /**
     * @return \AFE\Form\User
     */
    private function getUserForm() {
        /** @var \AFE\Form\User $form */
        $form = $this->serviceLocator->get('FormElementManager')->get('AFE\Form\User');
        $elem = $form->get('username');
        $elem->setAttribute('placeholder', '');
        $elem->setOption('column-size', 'sm-12');
        return $form;
    }
}
