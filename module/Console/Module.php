<?php
namespace Console;

use AFE\Model\User;
use Zend\Console\Adapter\AdapterInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ConsoleBannerProviderInterface;
use Zend\ModuleManager\Feature\ConsoleUsageProviderInterface;

class Module implements ConsoleBannerProviderInterface, ConsoleUsageProviderInterface, ConfigProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Returns a string containing a banner text, that describes the module and/or the application.
     * The banner is shown in the console window, when the user supplies invalid command-line parameters or invokes
     * the application with no parameters.
     *
     * The method is called with active Zend\KotkaConsole\Adapter\AdapterInterface that can be used to directly access KotkaConsole and send
     * output.
     *
     * @param  AdapterInterface $console
     * @return string|null
     */
    public function getConsoleBanner(AdapterInterface $console)
    {
        return
            "==------------------------------------------------------==" . PHP_EOL .
            "           Welcome to AFE commandline tool                " . PHP_EOL .
            "==------------------------------------------------------==" . PHP_EOL .
            "Version 0.0.1" . PHP_EOL;
    }

    /**
     * Returns an array or a string containing usage information for this module's console commands.
     * The method is called with active Zend\Console\Adapter\AdapterInterface that can be used to directly access
     * console and send output.
     *
     * If the result is a string it will be shown directly in the console window.
     * If the result is an array, its contents will be formatted to console window width. The array must
     * have the following format:
     *
     *     return array(
     *                'Usage information line that should be shown as-is',
     *                'Another line of usage info',
     *
     *                '--parameter'        =>   'A short description of that parameter',
     *                '-another-parameter' =>   'A short description of another parameter',
     *                ...
     *            )
     *
     * @param  AdapterInterface  $console
     * @return array|string|null
     */
    public function getConsoleUsage(AdapterInterface $console)
    {
        return array(
            'USER MANAGEMENT',
            'user add <username> [options]' => 'adds new user to AFE',
            'user mod <username> [options]' => 'modifies user parameters in AFE',
            'user del <username>' => 'deletes the user',
            'user info <username>' => 'shows information about user',
            'user list' => 'Lists all the users currecn',
            'options',
            array('--password=', 'Sets users password', "This can be used to specify password for the user." .
                "If it's omitted when adding, random password will be used instead"),
            array('--firstname=','Sets users firstname', ''),
            array('--lastname=','Sets users lastname', ''),
            array('--email=','Sets users email', 'This email is used for all mails that AFE send'),
            array('--role=','Sets users role', 'If omitted when adding default role (' . User::DEFAULT_ROLE_LOGIN . ') will be given. Available roles are: ' . implode(', ',[User::ROLE_ADMIN, User::ROLE_EDITOR, User::ROLE_COLLABORATOR, User::ROLE_MEMBER]) . '.'),
        );
    }
}
