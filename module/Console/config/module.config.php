<?php
return array(
    'console' => array(
        'router' => array(
            'routes' => array(
                'user' => array(
                    'options' => array(
                        'route' => 'user <action> [<username>] [--password=] [--firstname=] [--lastname=] [--email=] [--role=]',
                        'defaults' => array(
                            'controller' => 'Console\Controller\User',
                            'action' => 'list'
                        )
                    )
                ),
                'test' => array(
                    'options' => array(
                        'route' => 'test [excel]:actoin <file>',
                        'defaults' => array(
                            'controller' => 'Console\Controller\Test',
                            'action' => 'excel'
                        )
                    )
                ),
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Console\Controller\User' => 'Console\Controller\UserController',
            'Console\Controller\Test' => 'Console\Controller\TestController'
        )
    )
);
